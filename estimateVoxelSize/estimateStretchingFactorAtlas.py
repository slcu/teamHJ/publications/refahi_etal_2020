import cPickle
import numpy as np

from atlasviewer.image import computeL1Width
from atlasviewer.avtiff import avimread, avimsave, voxelDimensionsFromTiffFile



segmentedImagePath_10h = "/FM1/segmentation_tiffs/10h_segmented.tif"
segmentedImagePath_40h = "FM1/segmentation_tiffs/40h_segmented.tif"
segmentedImagePath_96h = "/FM1/segmentation_tiffs/96h_segmented.tif"
segmentedImagePath_120h = "/FM1/segmentation_tiffs/120h_segmented.tif"
segmentedImagePath_132h = "/FM1/segmentation_tiffs/132h_segmented.tif"


#------------------------------------------------------------------------------ 
# 10h
#------------------------------------------------------------------------------ 

print 80 * "-"

t10hResolution = [0.2415319, 0.2415319, 0.24]
image, info= avimread(segmentedImagePath_10h)
image = np.swapaxes(image, 2, 0)
result = computeL1Width(image, backgroundLabel = 1, maxLabel = np.max(image), imageResolution = t10hResolution, checkCellsOnZLast = True)
cellsL1Thickness = result[3]


cellsTop_10h = [476 ,  485 ,  479 ,  504 ,  487 ,  494 ,  516 ,  519 ,  502 ,  506 ,  515 ,  489 ,  514 ,  502 ,  506 ,  519 ,  530 ,  532 ,  529 ,  530 ,  532 ,  514 ,  521 ,  521 ,  505 ,  473]
cellsSide_10h = [293 ,  285 ,  337 ,  385 ,  355 ,  408 ,  384 ,  352 ,  295 ,  196 ,  269 ,  197 ,  196 ,  322 ,  205 ,  265 ,  322 ,  205 ,  265 ,  211 ,  149 ,  67 ,  191] 

vals1_10h = [cellsL1Thickness[cid] for cid in cellsTop_10h]
vals2_10h = [cellsL1Thickness[cid] for cid in cellsSide_10h]

a1 = np.average(vals1_10h)
a2 = np.average(vals2_10h)
newEstimation_10h = a1/a2
previousEstimation_10h = 1.9015855997199997 

print "10h: ", "new estimation:", newEstimation_10h, ", previous estimation:", previousEstimation_10h, ", number of chosen cells on the top:", len(cellsTop_10h), ", number of chosen cells on the side:", len(cellsSide_10h), ", error:", np.abs(previousEstimation_10h - newEstimation_10h)/previousEstimation_10h * 100

#------------------------------------------------------------------------------ 
# 40h
#------------------------------------------------------------------------------ 

print 80 * "-"

t40hResolution = [0.2415319, 0.2415319, 0.24]
image, info= avimread(segmentedImagePath_40h)
image = np.swapaxes(image, 2, 0)
result = computeL1Width(image, backgroundLabel = 1, maxLabel = np.max(image), imageResolution = t40hResolution, checkCellsOnZLast = True)
cellsL1Thickness = result[3]   


cellsTop_40h = 415 ,  425 ,  421 ,  420 ,  428 ,  418 ,  406 ,  405 ,  398 ,  410 ,  420 ,  400 ,  398 ,  405 ,  398 ,  410 ,  400 ,  386 ,  400 ,  386 ,  408 ,  402 ,  401 ,  389 ,  390 ,  378 , 
cellsSide_40h = 187 ,  227 ,  195 ,  275 ,  276 ,  253 ,  135 ,  166 ,  46 ,  123 ,  134 ,  16 ,  20 ,  322 ,  221 ,  232 ,  149 ,  12 ,  23,  55 ,  70 ,  243 ,  261  

cellsTop_40h = set(cellsTop_40h) 
cellsSide_40h = set(cellsSide_40h)

vals1_40h = [cellsL1Thickness[cid] for cid in cellsTop_40h]
vals2_40h = [cellsL1Thickness[cid] for cid in cellsSide_40h]
 
a1 = np.average(vals1_40h)     
a2 = np.average(vals2_40h)
newEstimation_40h = a1/a2
previousEstimation_40 = 1.8753479100799999 

print "40h: ", "new estimation:", newEstimation_40h, ", previous estimation:", previousEstimation_40, ", number of chosen cells on the top:", len(cellsTop_40h), ", number of chosen cells on the side:", len(cellsSide_40h), ", error:", np.abs(previousEstimation_40 - newEstimation_40h)/previousEstimation_40 * 100

#------------------------------------------------------------------------------ 
# 96h
#------------------------------------------------------------------------------ 

print 80 * "-"

t96hResolution = [0.2361645, 0.2361645, 0.24]
image, info= avimread(segmentedImagePath_96h)
image = np.swapaxes(image, 2, 0)
result = computeL1Width(image, backgroundLabel = 1, maxLabel = np.max(image), imageResolution = t96hResolution, checkCellsOnZLast = True)
cellsL1Thickness = result[3]


cellsTop_96h = 429 ,  428 ,  424 ,  425 ,  427 ,  422 ,  416 ,  426 ,  423 ,  420 ,  415 ,  419 ,  414 ,  418 ,  421 ,  417 ,  412 
cellsSide_96h = 134, 266, 267, 149, 278, 279, 280, 282, 283, 28, 172, 175, 243, 310, 186, 192, 224, 66, 195, 209, 213, 215, 96, 101, 233, 234, 115, 116, 119, 122, 54, 11

cellsTop_96h = set(cellsTop_96h) 
cellsSide_96h = set(cellsSide_96h)


vals1_96h = [cellsL1Thickness[cid] for cid in cellsTop_96h]
vals2_96h = [cellsL1Thickness[cid] for cid in cellsSide_96h]
  
a1 = np.average(vals1_96h)
a2 = np.average(vals2_96h)
newEstimation_96 = a1/a2
previousEstimation_96 = 1.44122575116  
  
print "96h: ", "new estimation:", newEstimation_96, ", previous estimation:", previousEstimation_96, ", number of chosen cells on the top:", len(cellsTop_96h), ", number of chosen cells on the side:", len(cellsSide_96h), ", error:", np.abs(previousEstimation_96 - newEstimation_96)/previousEstimation_96 * 100



#------------------------------------------------------------------------------ 
# 120h
#------------------------------------------------------------------------------ 

print 80 * "-"

t120Resolution = [0.2361645, 0.2361645, 0.24]
image, info= avimread(segmentedImagePath_120h)
image = np.swapaxes(image, 2, 0)
result = computeL1Width(image, backgroundLabel = 1, maxLabel = np.max(image), imageResolution = t120Resolution, checkCellsOnZLast = True)
cellsL1Thickness = result[3]


cellsTop_120h = 919 ,  926 ,  923 ,  913 ,  921 ,  910 ,  902 ,  918 ,  912 ,  922 ,  920 ,  924 ,  915 ,  902 ,  894 ,  909 ,  911 ,  924 ,  915 ,  901 ,  907 ,  908 ,  899 ,  884 ,  869 ,
cellsSide_120h = 576 ,  522 ,  510 ,  531 ,  430 ,  463 ,  605 ,  489 ,  634 ,  605 ,  489 ,  560 ,  637 ,  683 ,  663 ,  739 ,  740 ,  779 ,  783 ,  726 ,  726 ,  757 ,  825 ,  788 ,  831 ,    

cellsTop_120h_second = 919 ,  925 ,  923 ,  913 ,  902 ,  921 ,  926 ,  922 ,  920 ,  924 ,  915 ,  910 ,  899 ,  908 ,  918 ,  907 ,  901 ,  912 ,  906 ,  911 ,  916 ,  909 ,  904 ,  891 ,  885 ,  887 ,  868 ,  892 ,  875 ,  845 ,  830 ,  847 ,  869 ,  884 ,   
cellsSide_120h_second = 704 ,  663 ,  634 ,  662 ,  605 ,  560 ,  522 ,  586 ,  554 ,  489 ,  461 ,  429 ,  463 ,  576 ,  510 ,  430 ,  363 ,  396 ,  357 ,  412 ,  295 ,  345 ,
 
cellsTop_120h = set(cellsTop_120h) 
cellsSide_120h = set(cellsSide_120h)

cellsTop_120h_second = set(cellsTop_120h_second) 
cellsSide_120h_second = set(cellsSide_120h_second)


vals1_120h = [cellsL1Thickness[cid] for cid in cellsTop_120h]
vals2_120h = [cellsL1Thickness[cid] for cid in cellsSide_120h]
   
vals1_120h_second = [cellsL1Thickness[cid] for cid in cellsTop_120h_second]
vals2_120h_second = [cellsL1Thickness[cid] for cid in cellsSide_120h_second]

   
a1 = np.average(vals1_120h)
a2 = np.average(vals2_120h)
newEstimation_120h = a1/a2

a1_second = np.average(vals1_120h_second)
a2_second = np.average(vals2_120h_second)
newEstimation_second = a1_second/a2_second


previousEstimation_120 = 1.40528260462    

print "120h: ", "new estimation:", newEstimation_120h, ", previous estimation:", previousEstimation_120, ", number of chosen cells on the top:", len(cellsTop_120h), ", number of chosen cells on the side:", len(cellsSide_120h), ", error:", np.abs(previousEstimation_120 - newEstimation_120h)/previousEstimation_120 * 100
print "120h: ", "second new estimation, :", newEstimation_second, ", previous estimation:", previousEstimation_120, ", number of chosen cells on the top:", len(cellsTop_120h_second), ", number of chosen cells on the side:", len(cellsSide_120h_second), ", error:", np.abs(previousEstimation_120 - newEstimation_second)/previousEstimation_120 * 100

#------------------------------------------------------------------------------ 
# 132h
#------------------------------------------------------------------------------ 

print 80 * "-"

t132hResolution = [0.1735086, 0.1735086, 0.17] # 132h
image, info= avimread(segmentedImagePath_132h)
image = np.swapaxes(image, 2, 0)
result = computeL1Width(image, backgroundLabel = 1, maxLabel = np.max(image), imageResolution = t132hResolution, checkCellsOnZLast = True)
cellsL1Thickness = result[3]



cellsTop_132h = 1340 ,  1328 ,  1325 ,  1309 ,  1329 ,  1332 ,  1331 ,  1317 ,  1304 ,  1323 ,  1330 ,  1334 ,  1324 ,  1319 ,  1327 ,  1346 ,  1336 ,  1337 ,  1370 ,  1313 ,  1330 ,  1334 ,  1324 ,  1319 ,  1333 ,  1327 ,
cellsSide_132h = 957 ,  861 ,  887 ,  977 ,  896 ,  852 ,  758 ,  882 ,  968 ,  715 ,  742 ,  1129 ,  1145 ,  1100 ,  1004 ,  996 ,  960 ,  899 ,  1126 ,  1192 ,  1004 ,  960 ,  899 ,  1118 ,  970 ,  1166 ,  1094 ,  1053 ,  939 ,  829 ,  707 ,  775 ,  1088 ,  1013 ,  947 ,  759 ,

cellsTop_132h = set(cellsTop_132h) 
cellsSide_132h = set(cellsSide_132h)

vals1_132h = [cellsL1Thickness[cid] for cid in cellsTop_132h]
vals2_132h = [cellsL1Thickness[cid] for cid in cellsSide_132h]
    
a1 = np.average(vals1_132h)        
a2 = np.average(vals2_132h)
previousEstimation_132h = 1.3567944502800002
newEstimation_132h = a1/a2
print "132h: ", "new estimation:", newEstimation_132h, ", previous estimation:", previousEstimation_132h, ", number of chosen cells on the top:", len(cellsTop_132h), ", number of chosen cells on the side:", len(cellsSide_132h), ", error:", np.abs(previousEstimation_132h - newEstimation_132h)/previousEstimation_132h * 100 


print 80 * "-"


value10h = 1.832 
value96h = 1.376
print "10h", previousEstimation_10h, newEstimation_10h, np.abs(previousEstimation_10h - value10h) / previousEstimation_10h *100 
print "40h", previousEstimation_40, newEstimation_40h
print "96h", previousEstimation_96, newEstimation_96, np.abs(previousEstimation_96 - value96h) / previousEstimation_96 *100


