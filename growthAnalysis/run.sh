# run for all flowers
for i in {1..5}; do ./run-grate-distrs.sh i; done
for i in {1..5}; do ./run-aniso-distrs.sh i; done

# run specific for flower 1
python -c 'import growth_control; growth_control.go()'
jupyter nbconvert --to notebook --execute grates.ipynb
jupyter nbconvert --to notebook --execute anisos.ipynb
