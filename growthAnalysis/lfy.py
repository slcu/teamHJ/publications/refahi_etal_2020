from collections import defaultdict
import _pickle as cPickle
from os.path import join
import common.lin as lin
import common.seg as seg
import selected_cells_t47 as s
import pr
import grates_lfy as grateslf
import grates
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math
from itertools import repeat
import growth_control as gc


def set_plot_params(fontsize=11):
    sns.set_style("white")
    plt.rc('xtick', labelsize=fontsize)
    plt.rc('ytick', labelsize=fontsize)
    plt.rc('axes', labelsize=fontsize)
    params = {'legend.fontsize': fontsize}
    plt.rcParams.update(params)


def plot_ts_q_(ts, d, lb="vals", bounds=(0, 1), txt=""):
    vmin, vmax = bounds

    xs = [c.pos.x for c in ts]
    ys = [c.pos.y for c in ts]
    zs = [c.pos.z for c in ts]
    cs = [d[c.cid] for c in ts]

    fig = plt.figure(figsize=(1.5, 1.5))
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(elev=-77, azim=-55)
    ax.scatter(xs, ys, zs, c=cs, cmap="coolwarm", alpha=0.9, s=7, vmin=vmin,
               vmax=vmax, edgecolors='black', linewidths=0.5)
    ax.set_title(txt)
    plt.axis('off')

    fout = "comb_regions/{lb}.png".format(lb=lb)
    fig.tight_layout()
    plt.savefig(fout, dpi=300, bbox_inches='tight')
    plt.show()


def convertLins(linD):
    ts = [0, 9, 17, 24, 32, 39, 47, 57]

    for t1, t2 in zip(ts, ts[1:]):
        print(t1, t2)
        linFn_ = "daughters_mothers_{t1}h_{t2}h.lin"
        linFn = join(linD, linFn_.format(t1=str(t1),
                                         t2=str(t2)))

        with open(linFn, "rb") as linF:
            ln = cPickle.load(linF, encoding="latin1")

        x = defaultdict(list)

        for d, m in ln[0]:
            x[m].append(d)

        txtLinFn = "{t1}hrs_to_{t2}hrs.txt"
        with open(join(linD, txtLinFn.format(t1=str(t1), t2=str(t2))), "w") as fout:
            rs = list()
            for m, ds in x.items():
                dsRepr = ",".join([str(d) for d in ds])
                r = "{m}: {ds}".format(m=str(m), ds=dsRepr)
                rs.append(r)

            fout.write("\n".join(rs))


def read_filter_tss():
    tss, linss = lin.mkSeriesGeomIm("/Users/s1437009/Downloads/")

    cs_t47 = set.union(s.region_1,
                       s.region_2,
                       s.region_3,
                       s.region_4,
                       s.region_5)

    tss[47].L1_cells = cs_t47
    tss[47] = tss[47].filterL1()

    with open("t47_L1.csv", "w") as fout:
        fout.write(tss[47].toOrganism())


def cell_types():
    ctyps = dict()

    for cid in s.region_1:
        ctyps[cid] = 1

    for cid in s.region_2:
        ctyps[cid] = 2

    for cid in s.region_3:
        ctyps[cid] = 3

    for cid in s.region_4:
        ctyps[cid] = 4

    for cid in s.region_5:
        ctyps[cid] = 5

    return ctyps


def get_ctypes_():
    fns = ["t47_centre.csv",  # 1
           "t47_boundaries.csv",  # 2
           "t47_insepals.csv",  # 3
           "t47_outsepals1.csv"]  # 4

    ctyps = dict()

    for i, fn in enumerate(fns):
        d = pr.read_csv(fn)
        for cid in set([elem["id"] for elem in d]):
            ctyps[cid] = i + 1

    return ctyps


def get_ctypes_t39():
    tss, linss = lin.mkSeriesGeomIm("/Users/s1437009/Downloads/")
    ctyps = dict()
    
    for cid in s.region1_t39(linss):
        ctyps[cid] = 1

    for cid in s.region2_t39(linss):
        ctyps[cid] = 2

    for cid in s.region3_t39(linss):
        ctyps[cid] = 3

    for cid in s.region4_t39(linss):
        ctyps[cid] = 4

    for cid in s.region5_t39(linss):
        ctyps[cid] = 5

    return ctyps


def gratio_ts(grs, r1, r2):
    def gratio(xs, xs1):
        if not xs or not xs1:
            return np.nan

        m1 = np.median(xs)
        m2 = np.median(xs1)

        return (m1 - m2) / (m1 + m2)

    grs1 = [grs.get(cid, np.nan) for cid in r1]
    grs2 = [grs.get(cid, np.nan) for cid in r2]

    grs1_ = [gr for gr in grs1 if not math.isnan(gr)]
    grs2_ = [gr for gr in grs2 if not math.isnan(gr)]

    return gratio(grs1_, grs2_)


def plot_distrs(grs, r1, r2, ax):
    grs1 = dgets(grs, r1)
    grs2 = dgets(grs, r2)

    cls = sns.color_palette("coolwarm")

    ax.set_xlim(-0.05, 0.18)
    ax.set_ylim(0.0, 0.45)

    sns.histplot(data=grs1, bins=10, color=cls[-1], kde=True, ax=ax,
                 stat='probability')
    sns.histplot(data=grs2, bins=10, color=cls[0], kde=True, ax=ax,
                 stat='probability')
    ax.set_xlabel(r'growth rate ($h^{-1}$)')


def plot_distrs_(grs, grs_, r1, r2, r1_, r2_, ax, ax1):
    set_plot_params(fontsize=11)
    #ax1.set_title("lfy", fontsize=11)
    #ax.set_title("wt", fontsize=11)
    plot_distrs(grs, r1, r2, ax1)
    plot_distrs(grs_, r1_, r2_, ax)


def plot_distrs_strip(grs, r1, r2, lb):
    grs1 = [grs.get(cid, np.nan) for cid in r1]
    grs2 = [grs.get(cid, np.nan) for cid in r2]

    grs1_ = [gr for gr in grs1 if not math.isnan(gr)
             and gr > -0.005 and gr < 0.15]
    grs2_ = [gr for gr in grs2 if not math.isnan(gr)
             and gr > -0.005 and gr < 0.15]

    m1 = np.median(grs1_)
    m2 = np.median(grs2_)

    gr = (m1 - m2) / (m1 + m2)

    cls = sns.color_palette("coolwarm")

    fig = plt.figure(figsize=(4, 3))
    ax = fig.add_subplot('111')

    ax.set_xlim(-0.05, 0.18)

    ax.set_title("gr={:.3f}".format(gr))


    xs = list(repeat(1, len(grs1_))) + list(repeat(1, len(grs2_)))
    hs = list(repeat(1, len(grs1_))) + list(repeat(2, len(grs2_)))
    ys = grs1_ + grs2_
    sns.stripplot(xs, ys, hue=hs, ax=ax, alpha=0.25, palette={1: cls[0], 2:cls[-1]})
    sns.pointplot([1, 1], [m1, m2], hue=[1, 2], join=False,
                  markers="d", scale=.75, ci=None, ax=ax, palette={1: cls[0], 2:cls[-1]})

    ax.set_ylabel(r'$h^{-1}$'.format())
    plt.tight_layout()
    plt.savefig("comb_regions/zone{i}_strip.png".format(i=lb), dpi=300)


def plot_distrs_strip_(grs, grs_, r1, r2, r1_, r2_, lb):
    grs1 = [grs.get(cid, np.nan) for cid in r1]
    grs2 = [grs.get(cid, np.nan) for cid in r2]

    grs1_ = [gr for gr in grs1 if not math.isnan(gr)]
    grs2_ = [gr for gr in grs2 if not math.isnan(gr)]

    grs1_1 = [grs_.get(cid, np.nan) for cid in r1_]
    grs2_1 = [grs_.get(cid, np.nan) for cid in r2_]

    grs1_1 = [gr for gr in grs1_1 if not math.isnan(gr)]
    grs2_1 = [gr for gr in grs2_1 if not math.isnan(gr)]

    m1 = np.mean(grs1_)
    m2 = np.mean(grs2_)

    m1_ = np.mean(grs1_1)
    m2_ = np.mean(grs2_1)

    cls = sns.color_palette("coolwarm")

    fig = plt.figure(figsize=(4, 3))
    ax = fig.add_subplot('111')

    xs = (list(repeat("lfy", len(grs1_))) +
          list(repeat("lfy", len(grs2_))) +
          list(repeat("wt", len(grs1_1))) +
          list(repeat("wt", len(grs2_1))))
    hs = (list(repeat(1, len(grs1_))) +
          list(repeat(2, len(grs2_))) +
          list(repeat(1, len(grs1_1))) +
          list(repeat(2, len(grs2_1))))
    ys = grs1_ + grs2_ + grs1_1 + grs2_1
    sns.stripplot(xs, ys, hue=hs, ax=ax, alpha=0.25,
                  palette={1: cls[-1], 2: cls[0]})
    sns.pointplot([1, 1, 2, 2], [m1, m2, m1_, m2_],
                  hue=[1, 2, 1, 2], join=False,
                  markers="d", scale=.75, ci=None,
                  ax=ax, palette={1: cls[-1], 2: cls[0]})

    ax.set_xlabel(r'$\mu$m/h'.format())
    ax.set_xticklabels(["lfy", "wt"])
    ax.set_ylim(-0.05, 0.18)
    plt.savefig("comb_regions/zone{i}_strip.png".format(i=lb), dpi=300)
    plt.show()


def region_gratios_t39(tss, linss):
    r1_t39 = s.region1_t39(linss)
    r2_t39 = s.region2_t39(linss)
    r3_t39 = s.region3_t39(linss)
    r4_t39 = s.region4_t39(linss)
    r5_t39 = s.region5_t39(linss)

    cids_t39 = set.union(r1_t39,
                         r2_t39,
                         r3_t39,
                         r4_t39,
                         r5_t39)

    tss[39].L1_cells = cids_t39
    tss[39] = tss[39].filterL1()

    zone1 = (r1_t39, r5_t39)
    zone3 = (set.union(r1_t39, r2_t39), set.union(r3_t39, r4_t39))

    grsss = grates.grates_avg_cons(tss, linss)
    grss = grsss[39]

    for i, sep in enumerate([zone1, zone3]):
        p1, p2 = sep
        plot_distrs_strip(grss, p1, p2, "{i}_t{t}".format(i=str(i+1), t=39))

        gr = gratio_ts(grss, p1, p2)
        print(i+1, gr)

        d = dict()
        for c in tss[39]:
            if c.cid in p1:
                d[c.cid] = 1.0
            elif c.cid in p2:
                d[c.cid] = 0.0
            else:
                d[c.cid] = 0.5

        plot_ts_q_(tss[39], d, lb="zone{i}_t39_templ".format(i=i+1),
                   bounds=(0.1, 0.9))


def region_gratios_t47(tss, linss):
    r1_t47 = s.region_1
    r2_t47 = s.region_2
    r3_t47 = s.region_3
    r4_t47 = s.region_4
    r5_t47 = s.region_5

    cids_t47 = set.union(r1_t47,
                         r2_t47,
                         r3_t47,
                         r4_t47,
                         r5_t47)

    tss[47].L1_cells = cids_t47
    tss[47] = tss[47].filterL1()

    zone1 = (r1_t47, r5_t47)
    zone2 = (set.union(r1_t47, r2_t47),
             set.union(r3_t47, r4_t47, r5_t47))
    zone3 = (set.union(r1_t47, r2_t47), set.union(r3_t47, r4_t47))

    grss = grates.grates_backward(tss, linss, 39, 47)

    for i, sep in enumerate([zone1, zone2, zone3]):
        p1, p2 = sep
        plot_distrs_strip(grss, p1, p2, "{i}_t{t}".format(i=str(i+1), t=47))

        gr = gratio_ts(grss, p1, p2)
        print(i+1, gr)

        d = dict()
        for c in tss[47]:
            if c.cid in p1:
                d[c.cid] = 1.0
            elif c.cid in p2:
                d[c.cid] = 0.0
            else:
                d[c.cid] = 0.5

        plot_ts_q_(tss[47], d, lb="zone{i}_t47_templ".format(i=i+1),
                   bounds=(0.1, 0.9))


def boundary_gratios():
    tss, linss = lin.mkSeriesGeomIm("/Users/s1437009/Downloads/")

    r1_t47 = s.region_1
    r2_t47 = s.region_2
    r3_t47 = s.region_3
    r4_t47 = s.region_4
    r5_t47 = s.region_5

    cids_t47 = set.union(r1_t47,
                         r2_t47,
                         r3_t47,
                         r4_t47,
                         r5_t47)

    tss[47].L1_cells = cids_t47
    tss[47] = tss[47].filterL1()

    r1 = set.union(r1_t47, r2_t47)
    r2 = set.union(r3_t47, r4_t47)

    grss = grates.grates_backward(tss, linss, 39, 47)
    plot_distrs_strip(grss, r1, r2, "boundary")

    d = dict()
    for c in tss[47]:
        if c.cid in r1:
            d[c.cid] = 1.0
        elif c.cid in r2:
            d[c.cid] = 0.0
        else:
            d[c.cid] = 0.5

    plot_ts_q_(tss[47], d, lb="zone{i}_t47_templ".format(i="boundaries"),
               bounds=(0.1, 0.9))


def filterL1_lfy(tss, linss):
    r1_t32 = s.region1_t32(linss)
    r2_t32 = s.region2_t32(linss)
    r3_t32 = s.region3_t32(linss)
    r4_t32 = s.region4_t32(linss)
    r5_t32 = s.region5_t32(linss)

    cids_t32 = set.union(r1_t32,
                         r2_t32,
                         r3_t32,
                         r4_t32,
                         r5_t32)

    tss[32].L1_cells = cids_t32
    tss[32] = tss[32].filterL1()
    
    r1_t39 = s.region1_t39(linss)
    r2_t39 = s.region2_t39(linss)
    r3_t39 = s.region3_t39(linss)
    r4_t39 = s.region4_t39(linss)
    r5_t39 = s.region5_t39(linss)

    cids_t39 = set.union(r1_t39,
                         r2_t39,
                         r3_t39,
                         r4_t39,
                         r5_t39)

    tss[39].L1_cells = cids_t39
    tss[39] = tss[39].filterL1()

    r1_t47 = s.region_1
    r2_t47 = s.region_2
    r3_t47 = s.region_3
    r4_t47 = s.region_4
    r5_t47 = s.region_5

    cids_t47 = set.union(r1_t47,
                         r2_t47,
                         r3_t47,
                         r4_t47,
                         r5_t47)

    tss[47].L1_cells = cids_t47
    tss[47] = tss[47].filterL1()


def do_gratios_lfy_wt(tss, linss, tss_, linss_):
    r1_t39 = s.region1_t39(linss)
    r2_t39 = s.region2_t39(linss)
    r3_t39 = s.region3_t39(linss)
    r4_t39 = s.region4_t39(linss)
    r5_t39 = s.region5_t39(linss)
    
    zone1 = (r1_t39, r5_t39)
    zone3 = (set.union(r1_t39, r2_t39), set.union(r3_t39, r4_t39))

    zone1_ = gc.GPairRegions(120, tss_[120], 'ETTIN', 'LFY').get_regions()
    zone3_ = gc.GPairRegions(120, tss_[120], 'AP1', 'LFY').get_regions()

    grss = grateslf.grates_avg_cons(tss, linss)
    grss_ = grates.grates_avg_cons(tss_, linss_)
    

    for i, (z, z_) in enumerate([(zone1, zone1_), (zone3, zone3_)]):
        print(i)
        r1, r2 = z
        r1_, r2_ = z_
        plot_distrs_strip_(grss[39], grss_[120], r1, r2, r1_, r2_,
                           "s3-comp-{i}".format(i=i))


def dgets(d, ks, df=np.nan):
    vs = [d.get(k, np.nan) for k in ks]

    return [v for v in vs if not math.isnan(v)]


def plot_ratios(hs_ratios, ls_ratios, annots):
    xs = [i for i, _ in enumerate(hs_ratios)]
    fig = plt.figure(figsize=(4.5, 2.7))
    ax = fig.add_subplot(111)

    cls = sns.color_palette("coolwarm")
    sns.scatterplot(xs, hs_ratios,
                    ax=ax, s=80, alpha=1.0, color=cls[-1])
    sns.scatterplot(xs, ls_ratios,
                    ax=ax, s=80, alpha=1.0, color=cls[0])
    sns.lineplot(x=[0.0, len(xs)], y=[1.0, 1.0], ax=ax, linewidth=0.5)
    ax.set_xticks(xs)
    ax.set_xticklabels(annots)
    ax.set_title("lfy/wt")
    ax.lines[0].set_linestyle("--")
    plt.savefig("hl_ratios_wt_lfy_medians.svg", dpi=300, bbox_inches='tight')
    plt.show()


def dtime(gr):
    return 1/gr * np.log(2)


def plot_means(hmeans, lmeans):
    fig = plt.figure(figsize=(2.7, 2.7))
    ax = fig.add_subplot(111)

    cls = sns.color_palette("coolwarm")
    xs1 = [hm for hm, hm_ in hmeans]
    ys1 = [hm_ for hm, hm_ in hmeans]

    xs2 = [lm for lm, lm_ in lmeans]
    ys2 = [lm_ for lm, lm_ in lmeans]

    sns.scatterplot(xs1, ys1, s=80, color=cls[-1], ax=ax)
    sns.scatterplot(xs2, ys2, s=80, color=cls[0], ax=ax)
    sns.lineplot(x=[-0.05, 0.1], y=[-0.05, 0.1], ax=ax, linewidth=0.5)
    ax.lines[0].set_linestyle("--")
    plt.savefig("comb_regions/hl_medians_wt_lfy.png", dpi=300)
    plt.show()


def do_comp_s3(tss, linss, tss_, linss_):
    filterL1_lfy(tss, linss)
    lin.filterL1_st(tss_)
    
    r1_t39 = s.region1_t39(linss)
    r2_t39 = s.region2_t39(linss)
    r3_t39 = s.region3_t39(linss)
    r4_t39 = s.region4_t39(linss)
    r5_t39 = s.region5_t39(linss)

    zones_s3 = [((r1_t39, r5_t39),
                 gc.GPairRegions(120, tss_[120], 'ETTIN', 'LFY').get_regions()),
                ((set.union(r1_t39, r2_t39), set.union(r3_t39)),
                 gc.GPairRegions(120, tss_[120], 'AP1', 'LFY').get_regions()),
                ((set.union(r1_t39, r2_t39), set.union(r3_t39, r4_t39, r5_t39)),
                 gc.GPairRegions(120, tss_[120], 'SEP1', 'LFY').get_regions())]

    annots = ["{i}-s3".format(i=i+1) for i, _ in enumerate(zones_s3)]

    fig, axs = plt.subplots(2, 3, sharex=True, sharey=True, figsize=(12, 5.5))
    for i, (z, z_) in enumerate(zones_s3):
        ax = axs[0][i]
        ax1 = axs[1][i]
        r1, r2 = z
        lfy.append(gratio_ts(grss[39], r1, r2))
        r1_, r2_ = z_
        wt.append(gratio_ts(grss_[120], r1_, r2_))

        plot_distrs_(grss[39], grss_[120], r1, r2, r1_, r2_,
                     ax, ax1)

        hm_wt = np.median(dgets(grss_[120], r1_))
        hm_lfy = np.median(dgets(grss[39], r1))

        lm_wt = np.median(dgets(grss_[120], r2_))
        lm_lfy = np.median(dgets(grss[39], r2))

        hmeans.append((hm_wt, hm_lfy))
        lmeans.append((lm_wt, lm_lfy))

        hs_ratios.append((hm_lfy / hm_wt))
        ls_ratios.append((lm_lfy / lm_wt))

        print("wt {wtr:.3f}".format(wtr=(hm_wt - lm_wt) / (hm_wt + lm_wt)))
        print("lfy {lfyr:.3f}".format(lfyr=(hm_lfy - lm_lfy) / (hm_lfy + lm_lfy)))

        print(("""Doubling times for {z}-s3, wt: {dt1:.3f}, {dt2:.3f}"""
               .format(z=i+1,
                       dt1=dtime(lm_wt),
                       dt2=dtime(hm_wt))))

        print(("""Doubling times for {z}-s3, lfy: {dt1:.3f}, {dt2:.3f}"""
               .format(z=i+1,
                       dt1=dtime(lm_lfy),
                       dt2=dtime(hm_lfy))))

    for ax, txt in zip(axs[0], ["combination 1", "combination 2", "combination 3"]):
        ax.set_title(txt, fontsize=11)

    pad = 8  # in points
    for ax, row in zip(axs[:, 0], ["wt", "lfy"]):
        ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    fontsize=14, ha='right', va='center')

    fig.suptitle("stage 3", fontsize=13)
    fig.tight_layout()
    fig.subplots_adjust(wspace=0.15, top=0.88)
    plt.savefig("zone3-hists.svg", dpi=300, bbox_inches='tight')

    

def do_comp(tss, linss, tss_, linss_):
    filterL1_lfy(tss, linss)
    lin.filterL1_st(tss_)

    r1_t47 = s.region_1
    r2_t47 = s.region_2
    r3_t47 = s.region_3
    r4_t47 = s.region_4
    r5_t47 = s.region_5
    r6_t47 = s.region_6

    zones_s4 = [((r1_t47, r5_t47),
                 gc.GPairRegions(132, tss_[132], 'ETTIN', 'LFY').get_regions()),
                ((set.union(r1_t47, r2_t47), set.union(r3_t47)),
                 gc.GPairRegions(132, tss_[132], 'AP1', 'LFY').get_regions()),
                ((set.union(r1_t47, r2_t47), set.union(r3_t47, r4_t47, r5_t47)),
                 gc.GPairRegions(132, tss_[132], 'SEP1', 'LFY').get_regions()),
                ((set.union(r2_t47), set.union(r3_t47, r4_t47, r5_t47)),
                 gc.GPairRegions(132, tss_[132], 'MP', 'LFY').get_regions()),
                ((set.union(r2_t47), set.union(r5_t47)),
                 gc.GPairRegions(132, tss_[132], 'REV', 'LFY').get_regions()),
                ((set.union(r1_t47, r2_t47), set.union(r5_t47).difference(r6_t47)),
                 gc.GPairRegions(132, tss_[132], 'ANT', 'LFY').get_regions())]


    annots = ["{i}".format(i=i+1) for i, _ in enumerate(zones_s4)]
    
    grss = grateslf.grates_avg_cons(tss, linss)
    grss_ = grates.grates_avg_cons(tss_, linss_)
    grss_t47 = grates.grates_backward(tss, linss, 39, 47)

    lfy = list()
    wt = list()

    hs_ratios = list()
    ls_ratios = list()

    hmeans = list()
    lmeans = list()

    n = len(zones_s4)
    fig_w = n * 3
    fig, axs = plt.subplots(2, n, sharex=True, sharey=True, figsize=(fig_w, 5.5))
    for i, (z, z_) in enumerate(zones_s4):
        ax = axs[0][i]
        ax1 = axs[1][i]
        r1, r2 = z
        lfy.append(gratio_ts(grss[47], r1, r2))
        r1_, r2_ = z_
        wt.append(gratio_ts(grss_[132], r1_, r2_))
        plot_distrs_(grss_t47, grss_[132], r1, r2, r1_, r2_,
                     ax, ax1)

        hm_wt = np.median(dgets(grss_[132], r1_))
        hm_lfy = np.median(dgets(grss_t47, r1))

        lm_wt = np.median(dgets(grss_[120], r2_))
        lm_lfy = np.median(dgets(grss_t47, r2))

        hmeans.append((hm_wt, hm_lfy))
        lmeans.append((lm_wt, lm_lfy))

        hs_ratios.append((hm_lfy / hm_wt))
        ls_ratios.append((lm_lfy / lm_wt))

        print("wt {wtr:.3f}".format(wtr=(hm_wt - lm_wt) / (hm_wt + lm_wt)))
        print("lfy {lfyr:.3f}".format(lfyr=(hm_lfy - lm_lfy) / (hm_lfy + lm_lfy)))

        print(("""Doubling times for {z}-s4, wt: {dt1:.3f}, {dt2:.3f}"""
               .format(z=i+1,
                       dt1=dtime(lm_wt),
                       dt2=dtime(hm_wt))))

        print(("""Doubling times for {z}-s4, lfy: {dt1:.3f}, {dt2:.3f}"""
               .format(z=i+1,
                       dt1=dtime(lm_lfy),
                       dt2=dtime(hm_lfy))))


    lbs = ["combination {i}".format(i=i) for i, _ in enumerate(zones_s4)]
    for ax, txt in zip(axs[0], lbs):
        ax.set_title(txt, fontsize=11)

    pad = 8  # in points
    for ax, row in zip(axs[:, 0], ["wt", "lfy"]):
        ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    fontsize=14, ha='right', va='center')

    fig.suptitle("stage 4", fontsize=13)
    fig.tight_layout()
    fig.subplots_adjust(wspace=0.15, top=0.88)
    plt.savefig("zone4-hists.svg", dpi=300, bbox_inches='tight')

    fig = plt.figure(figsize=(3.5, 2.7))
    ax = fig.add_subplot('111')
    sns.scatterplot(wt, lfy, ax=ax, s=40, alpha=1.0)
    ax.set_xlabel("wt")
    ax.set_ylabel("lfy")
    ax.set_xlim(0.0, 0.5)
    ax.set_ylim(0.0, 0.5)
    ax.set_xticks([0.0, 0.25, 0.5])
    ax.set_yticks([0.0, 0.25, 0.5])
    sns.lineplot(x=[0.0, 0.5], y=[0.0, 0.5], ax=ax, linewidth=0.5)
    ax.lines[0].set_linestyle("--")

    for annot, x, y in zip(annots, wt, lfy):
        ax.annotate(annot, (x, y), fontsize=9)

    fig.tight_layout()
    plt.savefig("gratios_wt_lfy_medians.svg",
                dpi=300, bbox_inches='tight')

    plot_ratios(hs_ratios, ls_ratios, annots)

def go():
    tss, linss = lin.mkSeriesIm0(dataDir="../data/leafy")
    tss_, linss_ = lin.mkSeriesIm0(dataDir="../data/wt")

    do_comp(tss, linss, tss_, linss_)
