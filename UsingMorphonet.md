How to use morphonet?

1. Connect to: www.morphonet.org (note: not compatible with 'Safari')

2. Click on one of the following datasets (   )

reduced set: 6 main developmental stages with expression patterns (first one without). second set: 5 stages with expression patterns plus 13 additional stages without patterns.

3. For visualization see 'Navigation help' top left of screen:
- 'control' key + mouse to rotate the meristem
- 'option' key + mouse to move it up down, right or left
- use scroll bar on Morphonet menu to go from one time point to the other - to visualize the inner cells, go to 'Dataset' menu and use 'crop' function
- to change background colour go to 'movie'

4. To visualize cell lineage
- click on 1 cell and then move the scroll bar to another time point 

5. To visualize expression patterns
- Go to (i) time point 1-5 in the reduced set or (ii) go to time point 1, 5, 12, 15 or 17 in the complete set.
- In the 'genetic' menu click on one or more of the colored squares next to the gene names
- cells co-expressing several genes will start to flash. They can be selected and given a separate color using the 'object' menu.

6. To visualize cell volume

Go to 'Infos' open 'Quantitative', click on rectangle to choose colormap. Clicking on single cells will provide volume in μm3.
