############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are not permitted.
#
############################################################################

import os
import vtk
import numpy as np
# from skimage import measure
from atlasviewer.mcTables import edgeTable, triTable

class XYZ(object):
    def __init__(self, xyz, voxelSize = np.array([1, 1, 1])) :
        self.XYZ = np.array(xyz) * voxelSize

    def __str__(self):
        return str(self.XYZ)
    
    def __sub__(self, other):
        return self.XYZ - other.XYZ
    
    def __lt__(self, right):
        if self.XYZ[0] < right.XYZ[0]:
            return True
        elif self.XYZ[0] > right.XYZ[0]:
            return False
        
        if self.XYZ[1] < right.XYZ[1]:
            return True
        elif self.XYZ[1] > right.XYZ[1]:
            return False
        
        if self.XYZ[2] < right.XYZ[2]:
            return True
        elif self.XYZ[2] > right.XYZ[2]:
            return False
        
        return False
    
    def __add__(self, other):
        return self.XYZ + other.XYZ
    
    def __sub__(self, other):
        return self.XYZ - other.XYZ
        
    
class TRIANGLE(object):
    def __init__(self, point0, point1, point2):
        self.p0 = point0
        self.p1 = point1
        self.p2 = point2
    
    def __strold__(self):
        return "{p0: " + XYZ.__str__(self.p0) + ", p1: "+ XYZ.__str__(self.p1) + ", p2: " + XYZ.__str__(self.p2) + "}"
    
    def __str__(self):
        return "{p0: " + str(self.p0) + ", p1: "+ str(self.p1) + ", p2: " + str(self.p2) + "}"
    
    def area(self):
        a = np.sqrt(((self.p0 - self.p1) ** 2).sum())
        b = np.sqrt(((self.p0 - self.p2) ** 2).sum())
        c = np.sqrt(((self.p2 - self.p1) ** 2).sum())
        s = (a + b + c) / 2.0
        return np.sqrt( s * (s - a) * (s - b) * (s - c))
        

def trianglesArea(triangles):
    return sum([t.area() for t in triangles])
    
    
class GRIDCELL(object):
    def __init__(self, points, values):
#         p0, p1, p2, p3, p4, p5, p6, p7
        self.p = points
        self.val = values


def vertexInterp(isolevel, p1, p2, valp1, valp2):
    if p2 < p1:
        temp = p1
        p1 = p2
        p2 = temp
    if np.abs(valp1 - valp2) > 0.00001:
        return p1.XYZ + (p2 - p1)/(valp2 - valp1)*(isolevel - valp1) 
    else:
        return p1
    
    
def polygonise(grid, isolevel):
    cubeindex =  0
    vertlist = [XYZ([0, 0, 0]) for i in xrange(12)]
    triangles = []
    if grid.val[0] < isolevel:
        cubeindex |= 1
    if grid.val[1] < isolevel:
        cubeindex |= 2
    if grid.val[2] < isolevel:
        cubeindex |= 4
    if grid.val[3] < isolevel:
        cubeindex |= 8
    if grid.val[4] < isolevel:
        cubeindex |= 16
    if grid.val[5] < isolevel:
        cubeindex |= 32
    if grid.val[6] < isolevel:
        cubeindex |= 64
    if grid.val[7] < isolevel:
        cubeindex |= 128
        
    if (edgeTable[cubeindex] & 1):
        vertlist[0] = vertexInterp(isolevel,grid.p[0],grid.p[1],grid.val[0],grid.val[1])
    if (edgeTable[cubeindex] & 2):
        vertlist[1] = vertexInterp(isolevel,grid.p[1],grid.p[2],grid.val[1],grid.val[2])
    if (edgeTable[cubeindex] & 4):
        vertlist[2] = vertexInterp(isolevel,grid.p[2],grid.p[3],grid.val[2],grid.val[3])
    if (edgeTable[cubeindex] & 8):
        vertlist[3] = vertexInterp(isolevel,grid.p[3],grid.p[0],grid.val[3],grid.val[0])
    if (edgeTable[cubeindex] & 16):
        vertlist[4] = vertexInterp(isolevel,grid.p[4],grid.p[5],grid.val[4],grid.val[5])
    if (edgeTable[cubeindex] & 32):
        vertlist[5] = vertexInterp(isolevel,grid.p[5],grid.p[6],grid.val[5],grid.val[6])
    if (edgeTable[cubeindex] & 64):
        vertlist[6] = vertexInterp(isolevel,grid.p[6],grid.p[7],grid.val[6],grid.val[7])
    if (edgeTable[cubeindex] & 128):
        vertlist[7] = vertexInterp(isolevel,grid.p[7],grid.p[4],grid.val[7],grid.val[4])
    if (edgeTable[cubeindex] & 256):
        vertlist[8] = vertexInterp(isolevel,grid.p[0],grid.p[4],grid.val[0],grid.val[4])
    if (edgeTable[cubeindex] & 512):
        vertlist[9] = vertexInterp(isolevel,grid.p[1],grid.p[5],grid.val[1],grid.val[5])
    if (edgeTable[cubeindex] & 1024):
        vertlist[10] = vertexInterp(isolevel,grid.p[2],grid.p[6],grid.val[2],grid.val[6])
    if (edgeTable[cubeindex] & 2048):
        vertlist[11] = vertexInterp(isolevel,grid.p[3],grid.p[7],grid.val[3],grid.val[7])

    i = 0
    while triTable[cubeindex][i]!=-1:
        t = TRIANGLE(vertlist[triTable[cubeindex][i  ]], vertlist[triTable[cubeindex][i+1]], vertlist[triTable[cubeindex][i+2]])
        triangles.append(t)
        i+=3
    
    return triangles


def makeLookupTables(voxelSize):
    p0 = XYZ([0, 0, 0], voxelSize)
    p1 = XYZ([1, 0, 0], voxelSize)
    p2 = XYZ([1, 1, 0], voxelSize)
    p3 = XYZ([0, 1, 0], voxelSize)
    p4 = XYZ([0, 0, 1], voxelSize)
    p5 = XYZ([1, 0, 1], voxelSize)
    p6 = XYZ([1, 1, 1], voxelSize)
    p7 = XYZ([0, 1, 1], voxelSize)
    
    trianglesTables = {}
    contributionTables = {}
    points = [p0, p1, p2, p3, p4, p5, p6, p7]
    s = 0
    for v0 in [0, 1]:
        for v1 in [0, 1]:
            for v2 in [0, 1]:
                for v3 in [0, 1]:
                    for v4 in [0, 1]:
                        for v5 in [0, 1]:
                            for v6 in [0, 1]:
                                for v7 in [0, 1]:
                                    gc = GRIDCELL(points, [v0, v1, v2, v3, v4, v5, v6, v7])
                                    triangles = polygonise(gc, 0.5)
                                    trianglesTables[(v0, v1, v2, v3, v4, v5, v6, v7)] = triangles
                                    contributionTables[(v0, v1, v2, v3, v4, v5, v6, v7)] = trianglesArea(triangles)
    
    return trianglesTables, contributionTables
                                    


def decimateP(t):
    verts, faces = decimate(t[1], t[2], reduction = t[3], smooth_steps = t[4])
    return t[0], measure.mesh_surface_area(verts, faces)

def decimate(points, faces, reduction=0.50, smooth_steps=25, scalars=[], save_vtk=False, output_vtk=''):
    vtk_points = vtk.vtkPoints()
    [vtk_points.InsertPoint(i, x[0], x[1], x[2]) for i,x in enumerate(points)]

    vtk_faces = vtk.vtkCellArray()
    for face in faces:
        vtk_face = vtk.vtkPolygon()
        vtk_face.GetPointIds().SetNumberOfIds(3)
        vtk_face.GetPointIds().SetId(0, face[0])
        vtk_face.GetPointIds().SetId(1, face[1])
        vtk_face.GetPointIds().SetId(2, face[2])
        vtk_faces.InsertNextCell(vtk_face)

    polydata = vtk.vtkPolyData()
    polydata.SetPoints(vtk_points)
    polydata.SetPolys(vtk_faces)
    
    # We want to preserve topology (not let any cracks form).
    # This may limit the total reduction possible.
    decimate = vtk.vtkDecimatePro()

    # Migrate to VTK6:
    # http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Replacement_of_SetInput
    # Old: 
    decimate.SetInput(polydata)
    # new decimate.SetInputData(polydata)

    decimate.SetTargetReduction(reduction)
    decimate.PreserveTopologyOn()


    # smoothing
    if smooth_steps > 0:
        smoother = vtk.vtkSmoothPolyDataFilter()

        # Migrate to VTK6:
        # http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Replacement_of_SetInput
        # Old: 
        smoother.SetInput(decimate.GetOutput())
        # new smoother.SetInputConnection(decimate.GetOutputPort())

        smoother.SetNumberOfIterations(smooth_steps)
        smoother.Update()
        out = smoother.GetOutput()

        # Migrate to VTK6:
        # http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Replacement_of_SetInput
        # Old:

    else:
        decimate.Update()
        out = decimate.GetOutput()

    # Extract decimated points, faces
    newPoints = [list(out.GetPoint(point_id)) for point_id in range(out.GetNumberOfPoints())]
    if out.GetNumberOfPolys() > 0:
        polys = out.GetPolys()
        pt_data = out.GetPointData()
        newFaces = [[int(polys.GetData().GetValue(j))
                  for j in range(i*4 + 1, i*4 + 4)]
                  for i in range(polys.GetNumberOfCells())]
        if scalars:
            scalars = [pt_data.GetScalars().GetValue(i)
                       for i in range(len(points))]
    else:
        newFaces = []
        scalars = []

    faces = np.array(newFaces, dtype = faces.dtype)
    points = np.array(newPoints, dtype = points.dtype)
        

    return points, faces
                 


def decimatePolydata(polydata, reduction=0.50, smooth_steps=25, scalars=[], save_vtk=False, output_vtk=''):
    # We want to preserve topology (not let any cracks form).
    # This may limit the total reduction possible.
    decimate = vtk.vtkDecimatePro()

    # Migrate to VTK6:
    # http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Replacement_of_SetInput
    # Old: 
    decimate.SetInput(polydata)
    # new decimate.SetInputData(polydata)

    decimate.SetTargetReduction(reduction)
    decimate.PreserveTopologyOn()


    # smoothing
    if smooth_steps > 0:
        smoother = vtk.vtkSmoothPolyDataFilter()

        # Migrate to VTK6:
        # http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Replacement_of_SetInput
        # Old: 
        smoother.SetInput(decimate.GetOutput())
        # new smoother.SetInputConnection(decimate.GetOutputPort())

        smoother.SetNumberOfIterations(smooth_steps)
        smoother.Update()
        out = smoother.GetOutput()

        # Migrate to VTK6:
        # http://www.vtk.org/Wiki/VTK/VTK_6_Migration/Replacement_of_SetInput
        # Old:

    else:
        decimate.Update()
        out = decimate.GetOutput()

    # Extract decimated points, faces

    return out
                           

def cleanMesh(points, faces, tolerance):

    #-------------------------------------------------------------------------
    # vtk points:
    #-------------------------------------------------------------------------
    vtk_points = vtk.vtkPoints()
    [vtk_points.InsertPoint(i, x[0], x[1], x[2]) for i,x in enumerate(points)]

    #-------------------------------------------------------------------------
    # vtk faces:
    #-------------------------------------------------------------------------
    vtk_faces = vtk.vtkCellArray()
    for face in faces:
        vtk_face = vtk.vtkPolygon()
        vtk_face.GetPointIds().SetNumberOfIds(3)
        vtk_face.GetPointIds().SetId(0, face[0])
        vtk_face.GetPointIds().SetId(1, face[1])
        vtk_face.GetPointIds().SetId(2, face[2])
        vtk_faces.InsertNextCell(vtk_face)

    #-------------------------------------------------------------------------
    # vtkPolyData:
    #-------------------------------------------------------------------------
    polydata = vtk.vtkPolyData()
    polydata.SetPoints(vtk_points)
    polydata.SetPolys(vtk_faces)
    
    
    clean = vtk.vtkCleanPolyData()
    clean.SetInput(polydata)
    clean.SetAbsoluteTolerance(tolerance)
    
    print "tolerance = ", clean.GetAbsoluteTolerance()
    
    out = clean.GetOutput()
    out.Update()
    
    newPoints = [list(out.GetPoint(point_id)) 
              for point_id in range(out.GetNumberOfPoints())]
    
    if out.GetNumberOfPolys() > 0:
        polys = out.GetPolys()
        pt_data = out.GetPointData()
        newFaces = [[int(polys.GetData().GetValue(j))
                  for j in range(i*4 + 1, i*4 + 4)]
                  for i in range(polys.GetNumberOfCells())]

    
    newPoints = np.array(newPoints, dtype = points.dtype)
    newFaces = np.array(newFaces, dtype = faces.dtype)

    return newPoints, newFaces
                             


if __name__ == "__main__":
    from time import time
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d.art3d import Poly3DCollection
    
    def sphereImage(shape, radius, resolution = [1, 1, 1], label = 1, rotationAngle = None):
        image = np.zeros(shape, dtype = np.int8)
        center = (image.shape[0] / 2, image.shape[1] /2, image.shape[2] /2)
        for x in xrange(image.shape[0]):
            for y in xrange(image.shape[1]):
                for z in xrange(image.shape[2]):
                    if ( (((x - center[0]) * resolution[0]) ** 2) + (((y - center[1]) * resolution[1])** 2) + (((z - center[2]) * resolution[2])** 2)  <= ((radius  ) ** 2) ):
                        image[x, y, z] = label
        return image

    def cylinderImage(shape, radius, height, resolution, label = 1):
        image = np.zeros(shape, dtype = np.int8)
        center = (image.shape[0] / 2, image.shape[1] /2, image.shape[2] /2)
        for x in xrange(image.shape[0]):
            for y in xrange(image.shape[1]):
                for z in xrange(center[2] - height // 2,  (center[2] + height // 2)):
                    if ( (((x - center[0]) * resolution[0]) ** 2) + (((y - center[1]) * resolution[1])** 2) <= ((radius  ) ** 2) ):
                        image[x, y, z] = label
        return image
    
    t1 = time()
    voxelSize = np.array([1, 1, 1])
    trianglesTables, contributionTables = makeLookupTables(voxelSize)
    
    radius = 50
#     image = sphereImage([200, 200, 200], radius, resolution = voxelSize)
    height = 50
    image = cylinderImage([200, 200, 200], radius, height, resolution= voxelSize)

#     image = np.zeros((20, 20, 20), dtype = np.int8)
#     image[5:15, 5:15, 5:15] = 1
    surfaceArea = 0
    verts = []
    faces = []
    vertexCounter = 0
    for i in xrange(0, image.shape[0] - 1):
        print i
        for j in xrange(0, image.shape[1] - 1):
            for k in xrange(1, image.shape[2] - 1):
                key = (image[i , j, k], image[i + 1, j , k], image[i + 1, j + 1, k], image[i, j + 1, k], image[i , j, k + 1], image[i + 1, j , k + 1], image[i + 1, j + 1, k + 1], image[i, j + 1, k + 1])
                if sum(key) not in [0, 8]:
                    indexes = np.array([i, j, k]) 
                    surfaceArea += contributionTables[key]    
                    for t in trianglesTables[key]:
                        verts.extend([t.p0 + indexes, t.p1 + indexes, t.p2 + indexes])
                        faces.append([vertexCounter, vertexCounter + 1, vertexCounter + 2])
                        vertexCounter += 3 
    
    verts = np.array(verts)
    faces = np.array(faces)

    print verts.shape
    print faces.shape
    
    newVerts, newFaces = cleanMesh(verts, faces, tolerance = 0.001)
    print newVerts.shape
    print newFaces.shape
    
    print surfaceArea, 4 * np.pi * radius * radius, surfaceArea /( 4 * np.pi * radius * radius), 6 * 9 * 9
    print surfaceArea, (2 * np.pi * radius * radius ) + 2 * np.pi * radius * height, surfaceArea / ((2 * np.pi * radius * radius ) + 2 * np.pi * radius * height) 
    print "Elapsed time = ", time() - t1    

    vertsM, facesM = measure.marching_cubes(image, 0.5)
     
    print "m", vertsM.shape
    print "m", facesM.shape
    
    vtkVerts, vtkfaces = decimate(newVerts, newFaces, reduction=0.0,  smooth_steps=200)
    
    measuredSurface = measure.mesh_surface_area(vtkVerts, vtkfaces)
    print "measuredSurface = ", measuredSurface, measuredSurface / ((2 * np.pi * radius * radius ) + 2 * np.pi * radius * height)
    
    verts = vtkVerts
    faces = vtkfaces
    
    print verts.shape
    print faces.shape
    

    fig = plt.figure(figsize=(10, 12))
    ax = fig.add_subplot(111, projection='3d')
    mesh = Poly3DCollection(verts[faces], alpha = 0.051, color = "r")
    ax.add_collection3d(mesh)
    ax.set_xlim(-1, image.shape[0] * voxelSize[0] + 2 )  # a = 6 (times two for 2nd ellipsoid)
    ax.set_ylim(-1, image.shape[1] * voxelSize[1] + 2)  # b = 10
    ax.set_zlim(-1, image.shape[2] * voxelSize[2] + 2)  # c = 16
    
    plt.show()
    
