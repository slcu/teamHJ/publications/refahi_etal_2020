############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# * Redistribution and use in source and binary forms, with or without
# * modification, are not permitted.
#
############################################################################

from atlasviewer.aviewer import * 
from pkg_resources import resource_filename, Requirement

if __name__ == "__main__":
    
    path = resource_filename("atlasviewer", "")
    pathL = path.split("/")
    path = ""
    for p in pathL[:-1]:
        path += p   
        path += "/"
    path += "data/"
    imFile = path + "test_segmentation_tvformat.tif"
    colorMapFile = path + "colorMap.cmp"


    app = QtGui.QApplication(sys.argv)    
    
    username = "Atlas user"
    useAtlasGlobalColorCodes = False   
    window = Viewer(username, imFile, magnification = 14, colorMapFile = colorMapFile, wallId=0, checkBackground=False, addCellWalls=True, tvOutput = False, useAtlasGlobalColorCodes = useAtlasGlobalColorCodes)
    window.renderInteractor.Initialize() # Need this line to actually show the render inside Qt
    window.show()
    sys.exit(app.exec_())
        