############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# * Redistribution and use in source and binary forms, with or without
# * modification, are not permitted.
#
############################################################################
import vtk
import numpy as np
import skimage.measure
import scipy.ndimage as nd
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
from atlasviewer import avtiff
from atlasviewer.image import extractL1L2, boundaryCells
from atlasviewer.yMarchingCubes import decimatePolydata 
from atlasviewer.avtiff import voxelDimensionsFromTiffFile
from atlasviewer.tvGraph import *



def dilation(slices):
    return [slice( max(0, s.start - 1), s.stop + 1) for s in slices ]

def mergeSlices(slices01, slices02):
    return [slice( min(slices01[i].start, slices02[i].start), max(slices01[i].stop, slices02[i].stop)) for i in xrange(len(slices01)) ]

def boundaryNeighbourVoxels(simage, label):
    img = (simage == label)
    dil = nd.binary_dilation(img)
    boundary = dil - img
    return simage[boundary]

def importData(volumeData, datatype=np.uint16): 
    dataImporter = vtk.vtkImageImport()
    dataImporter.SetImportVoidPointer(volumeData, volumeData.nbytes)
    if datatype == np.uint8:
        dataImporter.SetDataScalarTypeToUnsignedChar()
    elif datatype == np.uint16:
        dataImporter.SetDataScalarTypeToUnsignedShort()
    else:
        raise TypeError("Input image must be uint8 or uint16! "  
                            "Consider casting the image.")             
    dataImporter.SetNumberOfScalarComponents(1)
    dataImporter.SetDataExtent(0, volumeData.shape[0] - 1, 
                               0, volumeData.shape[1] - 1, 
                               0, volumeData.shape[2] - 1)
    dataImporter.SetWholeExtent(0, volumeData.shape[0] - 1, 
                                0, volumeData.shape[1] - 1, 
                                0, volumeData.shape[2] - 1)
    return dataImporter


def tvMeshSkimage(fileName, labelsToMesh = None, downSize = None, 
                  resolution = (1, 1, 1)):
    """
    Mesh every cell in the image using the marching cubes from skimage library 
    """
    image, tags = tvtiff.tiffread(fileName)
    image = np.swapaxes(image, 2, 0)    
    if downSize is not None:
        image = nd.interpolation.zoom(image, downSize, order = 0)
    cellsSlices = nd.find_objects(image)
    cellsSlices = dict(enumerate(cellsSlices, start = 1))
    if labelsToMesh is None :
        labels = set(np.unique(image))
        labels.remove(1)
    elif labelsToMesh == "L1":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L1)
    elif labelsToMesh == "L2":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L2)
    elif labelsToMesh == "L1L2":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L1)
        labels.update(L2)
    else:
        labels = labelsToMesh

    meshes = dict()
    if 0 in labels:
        labels.remove(0)
    for cid in labels:
        print cid
        slices = dilation(cellsSlices[cid])
        mask = np.array((image[slices] == cid), dtype = np.int)
#         mask = nd.binary_erosion(mask)
        cell = np.array(mask, dtype = np.uint8)
        vertices, faces = skimage.measure.marching_cubes(cell, 0.5, spacing = resolution)
        vertices += (np.array([s.start for s in slices]) * np.array(resolution))
        meshes[cid] = (vertices, faces)
    return meshes


def tvMeshVTK(fileName, labelsToMesh = None, downSize = None, 
              resolution = (1, 1, 1), reduction = 0, smooth_steps = 0, removeBoundaryCells = True):
    """
    Mesh every cell in the image using marching cubes from VTK 
    """
    print "removeBoundaryCells: ", removeBoundaryCells
    image, tags = tvtiff.tiffread(fileName)
    image = np.swapaxes(image, 2, 0)
    if downSize is not None:
        image = nd.interpolation.zoom(image, downSize, order = 0)
    cellsSlices = nd.find_objects(image)
    cellsSlices = dict(enumerate(cellsSlices, start = 1))
    if labelsToMesh is None :
        labels = set(np.unique(image))
        labels.remove(1)
    elif labelsToMesh == "L1":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L1)
    elif labelsToMesh == "L2":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L2)
    elif labelsToMesh == "L1L2":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L1)
        labels.update(L2)
    else:
        labels = labelsToMesh
    if removeBoundaryCells:
        print "b cells:", boundaryCells(image)
        labels = set(labels) - boundaryCells(image)
    meshes = dict()
    if 0 in labels:
        labels.remove(0)
    for cid in labels:
        slices = dilation(dilation(cellsSlices[cid]))
        cell = np.asfortranarray(image[slices] == cid, dtype = np.uint8)
        dataImporter = importData(cell, datatype = np.uint8)
        dataImporter.SetDataSpacing((resolution[0], resolution[1], 
                                     resolution[2] ))
        vtkMC = vtk.vtkDiscreteMarchingCubes()
        vtkMC.SetInput(dataImporter.GetOutput())
        vtkMC.GenerateValues(1, 0, 1)
        vtkMC.Update()
        polyData = vtkMC.GetOutput()
        vtkP = polyData.GetPoints()
        vertices = vtk_to_numpy(vtkP.GetData())
        vertices += (np.array([s.start for s in slices]) * np.array(resolution))
        polyData = decimatePolydata(polyData, reduction, smooth_steps)
        meshes[cid] = polyData 
    return meshes

    
def tvMeshImageVTK(image, labelsToMesh = None, downSize = None, 
              resolution = (1, 1, 1), reduction = 0, smooth_steps = 0, 
              removeBoundaryCells = True):
    """
    Mesh every cell in the image using marching cubes from VTK 
    """
    if downSize is not None:
        image = nd.interpolation.zoom(image, downSize, order = 0)
    cellsSlices = nd.find_objects(image)
    cellsSlices = dict(enumerate(cellsSlices, start = 1))
    if labelsToMesh is None :
        labels = set(np.unique(image))
        print labels
        labels.remove(1)
    elif labelsToMesh == "L1":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L1)
    elif labelsToMesh == "L2":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L2)
    elif labelsToMesh == "L1L2":
        L1, L2 = extractL1L2(image, backgroundLabel = 1)
        labels = set(L1)
        labels.update(L2)
    else:
        labels = labelsToMesh

    if removeBoundaryCells:
        labels = set(labels) - boundaryCells(image)
    
    meshes = dict()
    if 0 in labels:
        labels.remove(0)
    cellNb = float(len(labels))
    counter = 0
    for cid in labels:
        print cid
        slices = dilation(dilation(cellsSlices[cid]))
        cell = np.asfortranarray(image[slices] == cid, dtype = np.uint8)
        dataImporter = importData(cell, datatype = np.uint8)
        dataImporter.SetDataSpacing((resolution[0], resolution[1], 
                                     resolution[2] ))
        vtkMC = vtk.vtkDiscreteMarchingCubes()
        vtkMC.SetInput(dataImporter.GetOutput())
        vtkMC.GenerateValues(1, 0, 1)
        vtkMC.Update()
        polyData = vtkMC.GetOutput()
        
        vtkP = polyData.GetPoints()
        vertices = vtk_to_numpy(vtkP.GetData())
        vertices += (np.array([s.start for s in slices]) * np.array(resolution))
        polyData = decimatePolydata(polyData, reduction, smooth_steps)
        meshes[cid] = polyData 

        print "%0.2f is done"%(100 * counter / cellNb)
        counter += 1
        
    return meshes


class TissueAnalysis(object):
    
    def __init__(self, image, resolution, background = 1, removeBoundaryCells = False):
        self.image = image
        self.resolution = np.array(resolution)
        
        self.labels = set(np.unique(self.image)) 
#         self.labels.remove(1)
        if removeBoundaryCells:
            self.labels -= set(boundaryCells(image))
        self.findCells()
        self.cellsProperties = {cid : {} for cid in self.labels}
        self.meshes = dict()
        self.erodedImageMeshes = dict()
        self.background = background
        
    def makeGraph(self):
        print "Creating adjacency graph ..."
        self.findNeighbors()
        self.edges = []
        for v, N in self.cellsNeighbors.iteritems():
            self.edges.extend([(v, n) for n in N])         
        self.graph = TVGraph(self.labels, self.edges)
        for cid, prop in self.cellsProperties.iteritems():
            self.graph.vertices[cid].property.update(prop)
        return self.labels, self.edges ### attention added later
        
    def computeVolumes(self):
        print "Computing cell volume ..."
        vols = (np.bincount(self.image.flatten()) * (self.resolution[0] * self.resolution[1] * self.resolution[2]))
        for cid in self.labels:
            self.cellsProperties[cid]["volume"] = vols[cid]
        
        
    def findCells(self):
        print "finding cells ..."
        self.cellsSlices = nd.find_objects(self.image)
        self.cellsSlices = dict(enumerate(self.cellsSlices, start = 1))
        print "cells found!"
        
    def findNeighbors(self):
        print "finding cell neighbors ..."
        self.cellsNeighbors = dict()
        for cid, bb in self.cellsSlices.iteritems():
            try:
                dilatedSlices = dilation(bb)
                img = self.image[dilatedSlices]
            except Exception as inst:
                print inst
                print "except : ", cid
                img = self.image
            self.cellsNeighbors[cid] = list(np.unique(boundaryNeighbourVoxels(img, cid)))
        print "finding cell neighbors: done"
    
    def meshExtractCellProperties(self, labelsToMesh = None,  reduction = 0, smooth_steps = 0):
        """
        Mesh every cell in the image using marching cubes from VTK 
        """
        
        if labelsToMesh is None :
            labels = list(self.labels)
#             if 1 in labels:
#                 labels.remove(1)
        elif labelsToMesh == "L1":
            L1, L2 = extractL1L2(self.image, backgroundLabel = 1)
            labels = set(L1)
        elif labelsToMesh == "L2":
            L1, L2 = extractL1L2(self.image, backgroundLabel = 1)
            labels = set(L2)
        elif labelsToMesh == "L1L2":
            L1, L2 = extractL1L2(self.image, backgroundLabel = 1)
            labels = set(L1)
            labels.update(L2)
        else:
            labels = labelsToMesh
        
        if 0 in labels:
            labels.remove(0)
        cellNb = float(len(labels))
        counter = 0
        for cid in labels:
            slices = dilation(dilation(self.cellsSlices[cid]))
            mask = (self.image[slices] == cid)
            cell = np.asfortranarray(mask, dtype = np.uint8)
            dataImporter = importData(cell, datatype = np.uint8)
            dataImporter.SetDataSpacing((self.resolution[0], self.resolution[1], 
                                         self.resolution[2] ))
            vtkMC = vtk.vtkDiscreteMarchingCubes()
            vtkMC.SetInput(dataImporter.GetOutput())
            vtkMC.GenerateValues(1, 0, 1)
            vtkMC.Update()
            polyData = vtkMC.GetOutput()
            vtkP = polyData.GetPoints()
            vertices = vtk_to_numpy(vtkP.GetData())
            vertices += (np.array([s.start for s in slices]) * self.resolution)
            polyData = decimatePolydata(polyData, reduction, smooth_steps)
            self.meshes[cid] = polyData
            
            mProp = vtk.vtkMassProperties()
            mProp.SetInput(polyData)
            sArea, vol = mProp.GetSurfaceArea() ,mProp.GetVolume()
            if sArea != 0:
#                 sphericity = ((36 * np.pi * vol * vol)**(1/3.0)) / sArea
                sphericity = ((np.pi ** (1/3.0)) * (6 *  vol)**(2/3.0)) / sArea
            else:
                sphericity = np.inf
            center = nd.measurements.center_of_mass(cell)
            center += (np.array([s.start for s in slices]))
            center = center * self.resolution
            
            self.cellsProperties[cid].update({"surfaceArea": sArea, "meshVolume" : vol, "sphericity": sphericity, "center": center})
            print cid, self.cellsProperties[cid] 
            print "%0.2f is done"%(100 * counter / cellNb)
            counter += 1


    def meshErodeBoundaryCells(self, labelsToMesh = None,  reduction = 0, smooth_steps = 0):
        """
        Mesh every cell in the image using marching cubes from VTK 
        """
        
        if labelsToMesh is None :
            labels = list(self.labels)
#             if 1 in labels:
#                 labels.remove(1)
        elif labelsToMesh == "L1":
            L1, L2 = extractL1L2(self.image, backgroundLabel = 1)
            labels = set(L1)
        elif labelsToMesh == "L2":
            L1, L2 = extractL1L2(self.image, backgroundLabel = 1)
            labels = set(L2)
        elif labelsToMesh == "L1L2":
            L1, L2 = extractL1L2(self.image, backgroundLabel = 1)
            labels = set(L1)
            labels.update(L2)
        else:
            labels = labelsToMesh
        
        if 0 in labels:
            labels.remove(0)
        cellNb = float(len(labels))
        counter = 0
        
        imageErodedBoundary = np.array(self.image)
        imageErodedBoundary[0, ...] = self.background
        imageErodedBoundary[-1, ...] = self.background 
        imageErodedBoundary[:, 0, :] = self.background
        imageErodedBoundary[:,-1, :] = self.background
        imageErodedBoundary[:, :, 0] = self.background
        imageErodedBoundary[:, :, -1] = self.background
        
        for cid in labels:
            slices = dilation(dilation(self.cellsSlices[cid]))
            mask = (imageErodedBoundary[slices] == cid)
            cell = np.asfortranarray(mask, dtype = np.uint8)
            dataImporter = importData(cell, datatype = np.uint8)
            dataImporter.SetDataSpacing((self.resolution[0], self.resolution[1], 
                                         self.resolution[2] ))
            vtkMC = vtk.vtkDiscreteMarchingCubes()
            vtkMC.SetInput(dataImporter.GetOutput())
            vtkMC.GenerateValues(1, 0, 1)
            vtkMC.Update()
            polyData = vtkMC.GetOutput()
            vtkP = polyData.GetPoints()
            vertices = vtk_to_numpy(vtkP.GetData())
            vertices += (np.array([s.start for s in slices]) * self.resolution)
            polyData = decimatePolydata(polyData, reduction, smooth_steps)
            self.erodedImageMeshes[cid] = polyData
            
            print "%0.2f is done"%(100 * counter / cellNb)
            counter += 1



    def meshExtractContactArea(self, labelsToMesh = None,  reduction = 0, smooth_steps = 0):
        """
        Mesh every cell in the image using marching cubes from VTK 
        """
        borderCells = boundaryCells(self.image)
        borderCells.remove(1)
        
        labels = list(self.labels)
        self.contactArea = {}
        
        self.contactArea = {(e.source.key, e.target.key) : None for eid, e in self.graph.undirectedEdges.iteritems()}
        
        self.compare = []
        
        edgeNb = len(self.contactArea)
        counter = 0
        bad = set()
        
        
        for edge in self.contactArea:
            counter += 1
            print edge, float(counter) / edgeNb#, edgeNb
    
            cellId, neighborId = edge
#             if True: #neighborId != 1 and cellId != 1 and (cellId not in borderCells) and (neighborId not in borderCells):
            if True:#(cellId not in borderCells) and (neighborId not in borderCells):
                mergedBb = dilation(dilation(mergeSlices(self.cellsSlices[cellId], self.cellsSlices[neighborId])))
                newImage = (self.image[mergedBb] == cellId) | (self.image[mergedBb] == neighborId)
                cell = np.asfortranarray(newImage, dtype = np.uint8)
                dataImporter = importData(cell, datatype = np.uint8)
                dataImporter.SetDataSpacing((self.resolution[0], self.resolution[1], 
                                         self.resolution[2] ))
                
                vtkMC = vtk.vtkDiscreteMarchingCubes()
                vtkMC.SetInput(dataImporter.GetOutput())
                vtkMC.GenerateValues(1, 0, 1)
                vtkMC.Update()
                polyData = vtkMC.GetOutput()
                polyData = decimatePolydata(polyData, reduction, smooth_steps)
                
                mProp = vtk.vtkMassProperties()
                mProp.SetInput(polyData)
                mergedArea = mProp.GetSurfaceArea()
                self.contactArea[(cellId, neighborId)] = ((self.cellsProperties[cellId]["surfaceArea"] + self.cellsProperties[neighborId]["surfaceArea"]) - mergedArea) / 2.0
        
    def checkCleanContactArea(self, surfaceThreshold):
        self.contactAreaCleaned = {}
        for edge, cArea in self.contactArea.iteritems():
            if np.abs(cArea) < surfaceThreshold:
                self.contactAreaCleaned[edge] = 0
            else:
                self.contactAreaCleaned[edge] = cArea
            
                   
                

