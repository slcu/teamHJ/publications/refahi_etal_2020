############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# * Redistribution and use in source and binary forms, with or without
# * modification, are not permitted.
#
############################################################################
import os
import sys
import random
import cPickle
import gzip, pickle
import numpy as np
import matplotlib
import vtk
from time import time
import imp
import sip
sip.setapi('QString', 2) 
sip.setapi('QVariant', 2) 
import scipy.ndimage as nd
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from PyQt4 import QtCore, QtGui
import matplotlib.pyplot as plt
import atlasviewer.avinfo
from atlasviewer.avtiff import avimread, avimsave, voxelDimensionsFromTiffFile
from atlasviewer.image import (checkBackgroundImage, extractL1L2, rangeCorrespondance, 
                               boundaryCells, makepatternCellsValues, generateGeneColormap)
from atlasviewer.tvui import Ui_MainWindow


__version__ = atlasviewer.tvinfo.__version__
__author__ = atlasviewer.tvinfo.__author__
__email__ = atlasviewer.tvinfo.__email__

__doc__ == """
AtlasViewer to visualize FlowerAtlas data refahi_et_al_2020.
"""
     

class Parameters(object):
    """
    Parameters of the viewer
    """
    backgroundLabel = 1    # background label in the segmentation 
    backgroundOpacity = 0.0    # The opacity of background marked by 
    # backgroundLabel
    backgroundRGB = (0, 0, 0)    # background label color, when backgroundOpacity is 
    # set to zero, changing backgroundRGB does not affect anything.
    cmColorsNb = 128    # number of colors used to color the cells  
    cmShuffle = True    # generated colormap is shuffled if True
    differentColoredCellNb = 16    # to better distinguish adjacent cells, they 
    # should be assigned different color codes. To achieve this goal, we assign 
    # different color codes to a cluster of cell successive cell labels. 
    # differentColoredCellNb is the size of the cluster.
    newColormapCellNumber = 4096    # number of cells in the newly generated 
    # colormap
    cropImageShape = [(0, -1), (0, -1), (0, -1)]     
    renBackground = (0, 0, 0)    # background color
    logoText = "AtlasViewer"
    saveColormapPath = "./"    # path where generated colormap will be saved
    zoomFactor = 0.5  


class EmittingStream(QtCore.QObject):
    """
    Used to show stdout and stderr in tissueviewer
    """
    textWritten = QtCore.pyqtSignal(str)
    def write(self, text):
        self.textWritten.emit(str(text))


class Viewer(QtGui.QMainWindow):
    """
    A class which implements a 3D viewer of intensity and segmented images  
    with an integrated IPython widget. 

    file extensions:
    tif: the intensity and segmented files should be in tif format 
    cmp: colormap
    pat: pattern files
    
    The IPython widget has the following variables:
    "cellsToShow": list of cell ids, i.e. labels, to show
    "cellsToRemove": list of cell to remove 
    "cellsValues" : a dictionary whoses keys are cell ids. The values of the 
    dictionary will be used to color the cell using gene color map.
    "cellsToMesh" :list of the cells to mesh
    "minExpressionValue": minimum expression value that is used to color cells
     according to "cellsValues" 
    "maxExpressionValue": maximum expression value that is used to color cells
     according to "cellsValues"
    
    variables in ipython widget:
    "cidsSet" : set of segmented image labels, i.e. cell ids
    "maxLabel": maximum of cidSet
    "patterns": patterns in .pat when dropped 
    
    assigned character events: 
    't': removes the logo text, i.e. TissueViewer 
    'w': generate and pickle a colormap for $n$ cells, $n$ is the number of 
         cells in the current image, i.e. maxLabel
    'e': generate and pickle a colormap for $n$ cells,  
        $n$ = Parameters.newColormapCellNumber
    """
    
    def __init__(self, username, imageFile, magnification = 8, 
                 colorMapFile = None, wallId = 0, checkBackground = False, 
                 addCellWalls= True, dataSpacing = None, parent = None, 
                 tvOutput = False, useAtlasGlobalColorCodes = False):
        
        if tvOutput:
            sys.stdout = EmittingStream(textWritten = self.normalOutputWritten)
            sys.stderr = EmittingStream(textWritten = self.normalOutputWritten)
            
        QtGui.QMainWindow.__init__(self, parent)        
        self.startTime = time()
        self.username = username
        self.wallId = wallId
        self.addCellWalls = addCellWalls
        self.colorMapFile = colorMapFile        
        
        self.checkBackground = checkBackground    
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self, imageFile)
        
        self.useAtlasGlobalColorCodes = useAtlasGlobalColorCodes
        
        if self.addCellWalls:
            self.ui.cellWallsCBox.setChecked(True)
            
        self.magnification = magnification
        self.dataSpacing = dataSpacing
        
        self.snapshotNb = 0    # number of snapshots taken by TissueViewer
        self.volumeClipperPlaneAdded = False
        self.sliceNb = 0
        self.clickedCellsToAdd = {}
        self.ui.GeneChilds = {}
        self.ui.userNamespace["allTagsD"] = {}
        
        self.progressBar = QtGui.QProgressBar()
        self.statusBar().addWidget(self.progressBar)
        self.progressBar.hide()
        
        self.renderer = vtk.vtkRenderer()
        self.renderer.SetViewport([0.0, 0.0, 1, 1])
        self.renderWin = self.ui.vtkWidget.GetRenderWindow()
        self.renderWin.AddRenderer(self.renderer)
        self.renderInteractor = self.renderWin.GetInteractor()
        self.renderInteractor.AddObserver("CharEvent", self.CharEventFunction)
        self.renderer.SetBackground(Parameters.renBackground)

        self.makeLogoText()
            
        self.colorLabelValues = [Parameters.backgroundLabel + 1, 
                                Parameters.cmColorsNb / 2, 
                                Parameters.cmColorsNb]
        
        if self.colorMapFile is None :
            self.makeNewColormap(Parameters.newColormapCellNumber)           
        else:
            self.colorMapFile = colorMapFile
            fobj = file(colorMapFile)
            self.colormap = np.array(cPickle.load(fobj))
            fobj.close()
        try:
            self.readFile(imageFile)
            self.showData()
        except IOError:
            print "File does not exist." 
        self.pushToIPythonShellNamespace(self.__dict__)
        
    def normalOutputWritten(self, text):
        """
        Append text to the QTextEdit used for stdout and stderr
        """
        # Maybe QTextEdit.append() works as well, but this is how I do it:
        cursor = self.ui.textOutputW.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        self.ui.textOutputW.setTextCursor(cursor)
        self.ui.textOutputW.ensureCursorVisible()  
    
    
    def makeLogoText(self):        
        # setting TissueViewer logo text
        self.logotxt = vtk.vtkTextActor()
        self.logotxt.SetTextScaleMode(0)
        self.logotxt.SetInput(Parameters.logoText)
        txtprop = self.logotxt.GetTextProperty()
        txtprop.SetFontFamilyToArial()
        txtprop.SetFontSize(14)
        txtprop.SetColor(1, 1, 1)
        self.logotxt.SetDisplayPosition(12, 12)
    
        
    def makeNewColormap(self, cellNbInCMap):
        """
        generate a numpy array with "CellNbInCMap" with dtype = np.uint8 
        Cell ids (labels) are indexes and values are color codes which are 
        numbers between 0 and 255. To better distinguish adjacent cells, 
        they should be assigned different color codes. To achieve this goal, 
        we assign different color codes to a cluster of cell successive cell 
        labels. Parameters.differentColoredCellNb is the size of the cluster.        
        """
        colorBasketNb = Parameters.cmColorsNb /Parameters.differentColoredCellNb
        colormapBase = np.array([], dtype=np.uint8, order="F")
        blockColorIndex = np.array([i * Parameters.differentColoredCellNb+ 2 
                                    for i in xrange(colorBasketNb)], 
                                   dtype=np.uint8, order='F')
        for j in xrange(Parameters.differentColoredCellNb):
            newBlockColorIndex = blockColorIndex + j
            if Parameters.cmShuffle:
                random.shuffle(newBlockColorIndex)
            colormapBase = np.concatenate([colormapBase, newBlockColorIndex])
        colormap = np.array(colormapBase, dtype=np.uint8, order='F')
        for i in xrange(int(np.ceil(
                                cellNbInCMap / (Parameters.cmColorsNb))) + 1):
            colormap = np.hstack([colormap, colormapBase])
        self.colormap = np.hstack([np.array([0, 1], dtype=np.uint8), colormap])
        
    def generatePickleColormap(self, maxCellNumber):
        """
        generate a new colormap array using makeNewColormap method and 
        pickle it.
        """
        self.makeNewColormap(maxCellNumber)
        fobj = file(Parameters.saveColormapPath + "newCM.cm", "w")
        cPickle.dump(self.colormap, fobj)
        fobj.close()
    
    def elapsedTime(self, message):
        """
        print the elpased time and set the startTime to current time
        """
        print message, time() - self.startTime
        self.startTime = time()
        
    
    def crop3DImage(self, image3D):
        if Parameters.cropImageShape[0][1] == -1:
            croppedImage = image3D[Parameters.cropImageShape[0][0]:, ...]
        else:
            croppedImage = image3D[Parameters.cropImageShape[0][0]:
                                   Parameters.cropImageShape[0][1], ...]
        if Parameters.cropImageShape[1][1] == -1:
            croppedImage = croppedImage[:, Parameters.cropImageShape[1][0]:, :]
        else:
            croppedImage = croppedImage[:, Parameters.cropImageShape[1][0]:
                                           Parameters.cropImageShape[1][1], :]
        if Parameters.cropImageShape[2][1] == -1:
            croppedImage = croppedImage[:, :, Parameters.cropImageShape[2][0]:]
        else:
            croppedImage = croppedImage[:, :, Parameters.cropImageShape[2][0]:
                                              Parameters.cropImageShape[2][1]]
        return croppedImage
        
        
    def readFile(self, imageFile = None):
        if imageFile is not None:
            self.imageFile = imageFile # This is used later for taking snapshots
        else:
            return "No image file to read."        
        self.image, self.imageTags = avimread(self.imageFile)
        if self.imageTags["type"] == "intensity":
            self.ui.intensityRadioButton.setChecked(True)
        elif self.imageTags["type"] == "segmented":
            self.ui.segmentationRadioButton.setChecked(True)            
        elif self.imageTags["type"] is None:
            print "Unable to identify image type, the type is set to intensity"
            self.ui.intensityRadioButton.setChecked(True)            
        self.image = np.swapaxes(self.image, 2, 0) 
            
        if self.dataSpacing is None:
            voxelDimensions = voxelDimensionsFromTiffFile(self.imageFile)
            voxelDimensions = [item if item is not None else 1 for item in voxelDimensions  ]
            self.dataSpacing = voxelDimensions
        
        self.ui.xSpinBox.setValue(self.dataSpacing[0])
        self.ui.ySpinBox.setValue(self.dataSpacing[1])
        self.ui.zSpinBox.setValue(self.dataSpacing[2])
        self.setWindowTitle(Parameters.logoText + ": "+ self.imageFile.split("/")[-1])
        self.image = np.asfortranarray(self.crop3DImage(self.image))
        self.imageCopy = np.copy(self.image)
        self.cids = set(np.unique(self.imageCopy))
        self.maxLabel = max(self.cids)
        self.elapsedTime(message = "Elapsed time to read the image = ")        
        
        screenAvailbaleGeom = QtGui.QDesktopWidget().availableGeometry()
        if self.magnification * self.image.shape[0] * self.dataSpacing[0] \
            < (screenAvailbaleGeom.right() - screenAvailbaleGeom.left()) and \
            self.magnification * self.image.shape[1] * self.dataSpacing[1]\
            < (screenAvailbaleGeom.bottom() - screenAvailbaleGeom.top()):
            self.resize(self.magnification * self.image.shape[0] * 
            self.dataSpacing[0], self.magnification * self.image.shape[1] 
            * self.dataSpacing[1])
        else:
            print "The requested screen size is bigger than the available",
            print "area the main screen, the window will be maximised."
            self.showMaximized()        
        if self.ui.segmentationRadioButton.isChecked() and self.checkBackground:
            print "Checking the background label ..."
            changed, result = checkBackgroundImage(self.image, Parameters.backgroundLabel)
            if changed:
                self.image = result
                self.maxLabel += 1
            self.elapsedTime(message = "Elapsed time to check the background = ")           
            
        if self.ui.cellWallsCBox.isChecked() and self.ui.segmentationRadioButton.isChecked():
            imageLaplace = np.array(nd.laplace(self.image), dtype = bool)
            self.image[imageLaplace] = self.wallId
            self.elapsedTime(message = "Elapsed time to add cell walls = ")
        self.pushToIPythonShellNamespace(self.__dict__)
    
    
    def pushToIPythonShellNamespace(self, varsDict):
        """push the variables in 'vars' into the ipython namespace
         varsDict: dictionary whose keys are variable names and values         
        """
        self.ui.ipython_kernel.shell.push(varsDict) 

    def showData(self, data=None):
        self.renderer.RemoveAllViewProps()
        if data is None:
            colorLabelValues = [Parameters.backgroundLabel + 1, 
                                self.maxLabel / 2, self.maxLabel]
            self.colorFunctions(colorLabelValues)
            self.importData(self.image)
        else:
            self.renderer.RemoveActor(self.volume)
            self.importData(data)
        self.createVolume()
        self.setVolumeProperty()
        if self.volumeClipperPlaneAdded:
            self.addVolumeClipperPlane()
        self.renderer.AddActor(self.logotxt)
        self.renderer.AddVolume(self.volume)
        self.renderWin.Render()  
        
    def colorFunctions(self, colorLabelValues, colorMapName = None):
        if colorMapName is None:
            colorMapName = str(self.ui.combo.currentText())        
        if self.ui.segmentationRadioButton.isChecked():
            self.alphaChannelFunc = vtk.vtkPiecewiseFunction()
            self.alphaChannelFunc.AddPoint(self.wallId, 
                                    self.ui.wallsOpacitySlider.value() / 100.0)
            self.alphaChannelFunc.AddPoint(Parameters.backgroundLabel, 
                                           Parameters.backgroundOpacity)
            self.alphaChannelFunc.AddPoint(colorLabelValues[0], 
                                    self.ui.colorsOpacitySlider.value() / 100.0)
            self.alphaChannelFunc.AddPoint(colorLabelValues[1], 
                                    self.ui.colorsOpacitySlider.value() / 100.0)
            self.alphaChannelFunc.AddPoint(colorLabelValues[2], 
                                    self.ui.colorsOpacitySlider.value() / 100.0)        
            self.colorFunc = vtk.vtkColorTransferFunction()        
            self.colorFunc.AddRGBPoint(self.wallId, self.ui.wallRGBValues[0], 
                                self.ui.wallRGBValues[1], self.ui.wallRGBValues[2])
            self.colorFunc.AddRGBPoint(Parameters.backgroundLabel, 
                                       Parameters.backgroundRGB[0],
                                       Parameters.backgroundRGB[1], 
                                       Parameters.backgroundRGB[2])
            if colorMapName == "RGB":
                self.colorFunc.AddRGBPoint(colorLabelValues[0], 1.0, 0.0, 0.0)
                self.colorFunc.AddRGBPoint(colorLabelValues[1], 0.0, 1.0, 0.0)
                self.colorFunc.AddRGBPoint(colorLabelValues[2], 0.0, 0.0, 1.0)
            else:
                normal = matplotlib.colors.Normalize(vmin=colorLabelValues[0], 
                                              vmax=colorLabelValues[2])
                colMap = plt.get_cmap(colorMapName)
                sm = matplotlib.cm.ScalarMappable(norm=normal, cmap=colMap)            
                for i in xrange(colorLabelValues[0], colorLabelValues[2] + 1):
                    cVals = sm.to_rgba(i)
                    self.colorFunc.AddRGBPoint(i, cVals[0], cVals[1], cVals[2])
        elif self.ui.intensityRadioButton.isChecked():
            self.alphaChannelFunc = vtk.vtkPiecewiseFunction()
            factor =  self.ui.wallsOpacitySlider.value() + 1
            alphaVector = np.array([0.0, 0.0005, 0.001, 0.025, 0.05, 0.1]) 
            alphaVector *= factor
            self.alphaChannelFunc.AddPoint(0, alphaVector[0])
            self.alphaChannelFunc.AddPoint(int(self.maxLabel * 0.05), 
                                           alphaVector[1])
            self.alphaChannelFunc.AddPoint(int(self.maxLabel * 0.25), 
                                           alphaVector[2])
            self.alphaChannelFunc.AddPoint(int(self.maxLabel * 0.5), 
                                           alphaVector[3])
            self.alphaChannelFunc.AddPoint(int(self.maxLabel * 0.75), 
                                           alphaVector[4])
            self.alphaChannelFunc.AddPoint(self.maxLabel, alphaVector[5])
            
            normal = matplotlib.colors.Normalize(vmin=0, vmax=self.maxLabel)
            colMap = plt.get_cmap(colorMapName)
            sm = matplotlib.cm.ScalarMappable(norm=normal, cmap=colMap)
            self.colorFunc = vtk.vtkColorTransferFunction()            
            for i in xrange(0, self.maxLabel + 1):
                cVals = sm.to_rgba(i)
                self.colorFunc.AddRGBPoint(i, cVals[0], cVals[1], cVals[2])
                
    def importData(self, volumeData): 
        self.dataImporter = vtk.vtkImageImport()
        self.dataImporter.SetImportVoidPointer(volumeData, volumeData.nbytes)
        datatype = volumeData.dtype
        if datatype == np.uint16:
            self.dataImporter.SetDataScalarTypeToUnsignedShort()
        elif datatype == np.uint8:
            self.dataImporter.SetDataScalarTypeToUnsignedChar()
        else:
            raise TypeError("Input image must be uint8 or uint16! "  
                            "Consider casting the image.")             
        self.dataImporter.SetNumberOfScalarComponents(1)
        self.dataImporter.SetDataExtent(0, volumeData.shape[0] - 1, 
                                        0, volumeData.shape[1] - 1, 
                                        0, volumeData.shape[2] - 1)
        self.dataImporter.SetWholeExtent(0, volumeData.shape[0] - 1, 
                                         0, volumeData.shape[1] - 1, 
                                         0, volumeData.shape[2] - 1)
        self.dataImporter.SetDataSpacing((self.ui.xSpinBox.value(), 
                                          self.ui.ySpinBox.value(), 
                                          self.ui.zSpinBox.value()))

    def createVolume(self):
        if self.ui.HDRendering.isChecked():
            self.volumeMapper = vtk.vtkVolumeTextureMapper2D()       
        else:
            self.volumeMapper = vtk.vtkVolumeRayCastMapper()
            self.volumeMapper.SetBlendModeToComposite            
            compositeFunction = vtk.vtkVolumeRayCastCompositeFunction()
            self.volumeMapper.SetVolumeRayCastFunction(compositeFunction)
        self.volumeMapper.SetInputConnection(self.dataImporter.GetOutputPort())
        self.volume = vtk.vtkVolume()
        self.volume.SetMapper(self.volumeMapper)
    
    
    def setVolumeProperty(self):
        self.volumeProperty = vtk.vtkVolumeProperty()
        self.volumeProperty.SetColor(self.colorFunc)
        self.volumeProperty.SetScalarOpacity(self.alphaChannelFunc)
        self.volumeProperty.SetInterpolationTypeToNearest()  
        self.volume.SetProperty(self.volumeProperty)


    def threeDAct(self):
        self.showData()
        
        
    def snapshot(self, snapshotSavePath = None):
        w2i = vtk.vtkWindowToImageFilter()
        w2i.SetInput(self.renderWin)
        png = vtk.vtkPNGWriter()
        png.SetInput(w2i.GetOutput())
        snapshotNbStr = str(self.snapshotNb)
        snapshotNbStr = (5 - len(snapshotNbStr)) * "0" + snapshotNbStr
        if snapshotSavePath is None:
            png.SetFileName("%s_snapshot_%d.png" %(str(self.imageFile[:-4]), 
                                                   self.snapshotNb))
        else:
            png.SetFileName("%s.png" %(str(snapshotSavePath)))
        png.Write()
        self.snapshotNb += 1


    def changeColormap(self):
        print "Wait while the new color map is being applied ..."
        self.colorFunctions(self.colorLabelValues)
        self.volumeProperty.SetColor(self.colorFunc)
        self.colorIndexImage = np.array(self.colormap[self.image], order='F')  
        self.showData(self.colorIndexImage)
                
    def showGeneExpression(self):
        expressionValues = np.zeros(self.maxLabel + 1)
        for k, v in self.ui.userNamespace["cellsValues"].iteritems():
            expressionValues[k] = v            
        
        
        thresholdValue = 0.001 # previous self.ui.thresholdSpinBox.value()
        cellIds = np.where(expressionValues >= thresholdValue)[0]
        
        colorLabelValues = [Parameters.backgroundLabel + 1, 
                            Parameters.cmColorsNb / 2 , Parameters.cmColorsNb]
        
        colorMapName = str(self.ui.combo_geneCM.currentText())
        self.colorFunctions(self.colorLabelValues, colorMapName)
        
        colormap = np.empty((self.maxLabel + 1,), dtype=np.uint8)
        colormap.fill(Parameters.backgroundLabel)
        
        maxValue = max(self.ui.userNamespace["cellsValues"].values())

        if self.ui.userNamespace["minExpressionValue"] is not None:
            minValue = self.ui.userNamespace["minExpressionValue"]
        else:
            minValue = min(self.ui.userNamespace["cellsValues"].values())
            
        if self.ui.userNamespace["maxExpressionValue"] is not None:
            maxValue = self.ui.userNamespace["maxExpressionValue"]
        else:
            maxValue = max(self.ui.userNamespace["cellsValues"].values())

        colormap[cellIds] = np.uint8(rangeCorrespondance(minValue, maxValue, 2, colorLabelValues[2], expressionValues[cellIds]))
        colormap[self.wallId] = 0 # cell walls id
        colormap[Parameters.backgroundLabel] = 1
        self.colorIndexImage = np.array(colormap[self.image], order='F')  
        # Attention: data must be np.uint8
        self.showData(self.colorIndexImage)
        self.makeColorBar(minValue, maxValue)
        self.renderWin.Render()
        

    def makeColorBar(self, minValue = None, maxValue = None):
        vals = self.ui.userNamespace["cellsValues"].values()
        if len(vals) == 0:
            print "No values for cells to show !"
        else:
            if (minValue is None) and (maxValue is None):
                valMin, valMax = min(vals), max(vals)
            else:
                valMin = minValue
                valMax = maxValue 
            self.colorbarColorFunc = vtk.vtkColorTransferFunction()
            norm = matplotlib.colors.Normalize(vmin = valMin, vmax = valMax)
            cmap = plt.get_cmap(str(self.ui.combo_geneCM.currentText()))
            self.m = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
            for i in np.linspace(valMin, valMax, Parameters.cmColorsNb):
                cVals = self.m.to_rgba(i)
                self.colorbarColorFunc.AddRGBPoint(i, cVals[0], cVals[1], cVals[2])
            colorMapBar = vtk.vtkScalarBarActor()
            colorMapBar.SetLookupTable(self.colorbarColorFunc)
            colorMapBar.GetLabelTextProperty().SetColor(0, 0, 1)
            coord = colorMapBar.GetPositionCoordinate()
            coord.SetCoordinateSystemToNormalizedViewport()
            coord.SetValue(0.05, 0.6)
            colorMapBar.SetWidth(0.06)
            colorMapBar.SetHeight(0.375)
            
            propT = vtk.vtkTextProperty()
            propL = vtk.vtkTextProperty()
            propT.SetFontFamilyToArial()
            propT.ItalicOff()
            propT.BoldOn()
            propL.BoldOff()
            colorMapBar.SetTitleTextProperty(propT)
            colorMapBar.SetLabelTextProperty(propL)
            
            self.renderer.AddActor(colorMapBar)
            self.renderWin.Render()        


    def showGeneExpressionAtlas(self):
        
        forL1 = {frozenset(['AHP6', 'AP2', 'AP1', 'ANT', 'ATML1', 'MP', 'SEP1', 'ETTIN', 'SEP2', 'LFY']): 8, 
                 frozenset(['AP2', 'STM', 'AP1', 'ANT', 'ATML1', 'MP', 'SEP1', 'ETTIN', 'SEP2', 'LFY']): 12, 
                 frozenset(['AS1', 'AP2', 'AP1', 'ANT', 'ATML1', 'SVP', 'MP', 'ETTIN', 'LFY', 'FIL']): 5, 
                 frozenset(['AS1', 'AP2', 'PHB', 'AP1', 'REV', 'ANT', 'ATML1', 'MP', 'SEP1', 'SEP2', 'LFY', 'PHV']): 18, 
                 frozenset(['AS1', 'AP2', 'AP1', 'ANT', 'ATML1', 'SEP1', 'ETTIN', 'SEP2', 'LFY', 'FIL']): 27, 
                 frozenset(['SEP3', 'AP3', 'AG', 'ATML1', 'STM', 'ANT', 'SEP1', 'MP', 'PHB', 'ETTIN', 'SEP2', 'PI', 'PHV']): 22, 
                 frozenset(['SEP3', 'AG', 'ATML1', 'AP3', 'CUC1', 'CUC3', 'CUC2', 'MP', 'STM', 'SEP1', 'SEP2', 'PI']): 25, 
                 frozenset(['AHP6', 'AS1', 'AP2', 'PHB', 'AP1', 'REV', 'ANT', 'ATML1', 'MP', 'SEP1', 'SEP2', 'LFY', 'PHV']): 26, 
                 frozenset(['PUCHI', 'PHB', 'REV', 'ANT', 'ATML1', 'MP', 'ETTIN', 'LFY', 'PHV']): 2, 
                 frozenset(['ANT', 'AS1', 'MP', 'ATML1', 'ETTIN', 'LFY', 'FIL']): 1, 
                 frozenset(['AG', 'ATML1', 'STM', 'REV', 'ANT', 'PHB', 'SEP1', 'MP', 'SUP', 'ETTIN', 'SEP2', 'SEP3', 'PHV']): 24, 
                 frozenset(['AS1', 'AP2', 'AP1', 'ANT', 'ATML1', 'MP', 'SEP1', 'ETTIN', 'SEP2', 'LFY', 'FIL']): 17, 
                 frozenset(['AP2', 'STM', 'AP1', 'MP', 'SEP1', 'SEP2']): 21, 
                 frozenset(['AG', 'ATML1', 'STM', 'REV', 'SEP1', 'MP', 'PHB', 'ETTIN', 'SEP2', 'SEP3', 'PHV']): 15, 
                 frozenset(['AS1', 'AP2', 'AP1', 'ANT', 'SEP1', 'SEP2', 'LFY', 'FIL']): 28, 
                 frozenset(['AP2', 'PHB', 'AP1', 'REV', 'ANT', 'ATML1', 'MP', 'STM', 'LFY', 'PHV', 'SVP']): 7, 
                 frozenset(['SEP1', 'AP2', 'STM', 'AP1', 'REV', 'ANT', 'ATML1', 'MP', 'PHB', 'ETTIN', 'SEP2', 'LFY', 'PHV']): 10, 
                 frozenset(['AS1', 'AP2', 'STM', 'AP1', 'ANT', 'ATML1', 'SVP', 'MP', 'SEP1', 'ETTIN', 'SEP2', 'LFY', 'FIL']): 9, 
                 frozenset(['AP2', 'AP3', 'AP1', 'CUC1', 'ATML1', 'CUC3', 'CUC2', 'MP', 'STM', 'SEP1', 'SEP2']): 30, 
                 frozenset(['AHP6', 'AS1', 'AP2', 'AP1', 'ANT', 'ATML1', 'MP', 'SEP1', 'ETTIN', 'SEP2', 'LFY', 'FIL']): 16, 
                 frozenset(['ATML1', 'PHB', 'REV', 'ANT', 'PUCHI', 'MP', 'LFY', 'PHV']): 3, 
                 frozenset(['AG', 'ATML1', 'STM', 'REV', 'SEP1', 'MP', 'PHB', 'ETTIN', 'SEP2', 'SEP3', 'CLV3', 'PHV']): 14, 
                 frozenset(['SEP3', 'AP3', 'AG', 'ATML1', 'STM', 'REV', 'SEP1', 'MP', 'PHB', 'ETTIN', 'SEP2', 'PI', 'PHV']): 13, 
                 frozenset(['AP2', 'PHB', 'AP1', 'REV', 'ANT', 'PUCHI', 'MP', 'STM', 'LFY', 'PHV', 'SVP']): 6, 
                 frozenset(['AP3', 'ATML1', 'STM', 'AP1', 'REV', 'CUC1', 'AP2', 'CUC3', 'CUC2', 'MP', 'PHB', 'SEP1', 'SEP2', 'PI', 'PHV']): 19, 
                 frozenset(['AP2', 'STM', 'AP1', 'CUC1', 'ATML1', 'CUC3', 'CUC2', 'MP', 'SEP1', 'SEP2']): 20, 
                 frozenset(['AG', 'ATML1', 'STM', 'REV', 'ANT', 'SEP1', 'MP', 'PHB', 'ETTIN', 'SEP2', 'SEP3', 'PHV']): 23, 
                 frozenset(['AP2', 'AP3', 'AP1', 'CUC1', 'ATML1', 'CUC3', 'CUC2', 'MP', 'STM', 'SEP1', 'SEP2', 'PI']): 29, 
                 frozenset(['AS1', 'ATML1', 'ANT', 'PUCHI', 'MP', 'ETTIN', 'LFY', 'FIL']): 31, 
                 frozenset(['AP2', 'AP1', 'ANT', 'ATML1', 'MP', 'SEP1', 'ETTIN', 'SEP2', 'LFY']): 11, 
                 frozenset(['CUC1', 'CUC3', 'CUC2', 'MP', 'ATML1', 'STM']): 4}

        allTagsD = forL1  
        
        allTagsD[frozenset(["no genes"])] = max(allTagsD.values()) + 1
        allTagsDOriginal = dict(allTagsD)
        
        # swapping codes to optimise patterns colors to improve the visual distinction of patterns. 
        newCodes = {
            1 : 2 , 
            2 : 27 , 
            3 : 16 , 
            4 : 11 , 
            5 : 8 , 
            6 : 25 , 
            7 : 1 , 
            8 : 14 , 
            9 : 22 , 
            10 : 24 , 
            11 : 13 , 
            12 : 4 , 
            13 : 20 , 
            14 : 6 , 
            15 : 18 , 
            16 : 9 , 
            17 : 21 , 
            18 : 15 , 
            19 : 12 , 
            20 : 5 , 
            21 : 7 , 
            22 : 26 , 
            23 : 10 , 
            24 : 3 , 
            25 : 23 , 
            26 : 17 , 
            27 : 1 , 
            28 : 28 ,
            29 : 29 ,  
            30 : 30 ,
            31 : 31 ,
            32 : 32 ,
            33 : 33 ,
            }

        # further swapping
        temp1 = newCodes[30]
        newCodes[30] = newCodes[17]
        newCodes[17] = temp1 
        
        temp1 = newCodes[14]
        newCodes[14] = newCodes[31]
        newCodes[31] = temp1
        
        temp1 = newCodes[4]
        newCodes[4] = newCodes[10]
        newCodes[10] = temp1
        
        
        newTagsD = dict()
        for k, v in allTagsD.iteritems():
            newTagsD[k] = newCodes[v]
        allTagsD = newTagsD
             
        self.ui.userNamespace["allTagsD"] = allTagsD
        
        if self.ui.genesItem.checkState(0) > 0:
            genesToShow = [geneName for geneName in self.genePatterns.keys()  if self.ui.GeneChilds[geneName].checkState(0) == QtCore.Qt.Checked]
            self.ui.userNamespace["cellsValues"], self.tagsColormapId, tagsCIds = makepatternCellsValues(self.genePatterns, genesToShow)
            cellsKeys = self.ui.userNamespace["cellsValues"].keys()
            notExpressingCells = self.cids - set(cellsKeys)
            
            self.ui.userNamespace["geneTagsCIds"] = tagsCIds
            
            if self.useAtlasGlobalColorCodes:
                self.ui.userNamespace["minExpressionValue"] = min(allTagsD.values())
                self.ui.userNamespace["maxExpressionValue"] = max(allTagsD.values())
                local2GlobalCode = {self.tagsColormapId[t]: allTagsD[t] for t in self.tagsColormapId}
                
                newCellsValue = {} 
                for cid, v in self.ui.userNamespace["cellsValues"].iteritems():
                    newCellsValue[cid] = local2GlobalCode[v]
                
                newTagsColormapId = {}
                for t, v in self.tagsColormapId.iteritems():
                    newTagsColormapId[t] = local2GlobalCode[v]
            
                self.ui.userNamespace["cellsValues"] = newCellsValue
                self.tagsColormapId = newTagsColormapId
                
            if self.useAtlasGlobalColorCodes:
                newLabel = max(allTagsD.values())
            else:
                newLabel = max(set(self.ui.userNamespace["cellsValues"].values())) + 1 
            
            for ncid in notExpressingCells:
                self.ui.userNamespace["cellsValues"][ncid] = newLabel #allTagsD[frozenset(["no genes"])]
                    
            self.tagsColormapId[frozenset(["no gene expression "])] = newLabel
            tagsCIds[frozenset(["no gene expression "])] = notExpressingCells
            
            self.showGeneExpression()
            
            if self.ui.userNamespace["minExpressionValue"] is not None:
                minValue = self.ui.userNamespace["minExpressionValue"]
            else:
                minValue = min(self.ui.userNamespace["cellsValues"].values())
                
            if self.ui.userNamespace["maxExpressionValue"] is not None:
                maxValue = self.ui.userNamespace["maxExpressionValue"]
            else:
                maxValue = max(self.ui.userNamespace["cellsValues"].values())
            
            self.genesNamesActors = {}
            all_codes = self.ui.userNamespace["cellsValues"].values()
            allTagsSorted = self.tagsColormapId.keys()
            allTagsSorted = [list(t) for t in allTagsSorted]
            allTagsSorted.sort()
            self.ui.userNamespace["geneTags"] = self.tagsColormapId 
            for ind in xrange(len(allTagsSorted)):
                tags = frozenset(allTagsSorted[ind])
                code = self.tagsColormapId[tags]
                cVals = self.m.to_rgba(code)
                self.genesNamesActors[tags] = [-1, vtk.vtkTextActor()]
                self.genesNamesActors[tags][1].SetTextScaleMode(0)
                cellsNbInPatCode =  all_codes.count(code)
                if allTagsSorted[ind] != ['no genesH']:
                    if frozenset(allTagsSorted[ind]) in self.ui.userNamespace["allTagsD"]:
                        globalCode = allTagsDOriginal[frozenset(allTagsSorted[ind])]
                        self.genesNamesActors[tags][0] = globalCode
                        tagStr = "code %d : "%(globalCode)
                    else:
                        tagStr = ""
                else:
                    tagStr = "Nb of cells = %d :"%(cellsNbInPatCode)
                tagsToSort = allTagsSorted[ind]
                tagsToSort.sort() 
                for item in tagsToSort:
                    tagStr += item
                    tagStr += ", "
                tagStr = tagStr[:-2]
                self.genesNamesActors[tags][1].SetInput(tagStr)
                txtprop = self.genesNamesActors[tags][1].GetTextProperty()
                txtprop.BoldOn()
                txtprop.SetFontSize(20)
                txtprop.SetColor(cVals[0], cVals[1], cVals[2])
                self.genesNamesActors[tags][1].SetDisplayPosition(12, 12 + (ind + 1) * 22)
        
        self.addTagsTextActor()
        self.renderWin.Render()
    
    def addTagsTextActor(self):
        toAdd = []
        for geneName, (code, gNameActor) in self.genesNamesActors.iteritems():
            toAdd.append((code, gNameActor))
        toAdd.sort(reverse = True)
        ind = 0
        for code, gNameActor in toAdd:
            gNameActor.SetDisplayPosition(12, 12 + (ind + 1) * 30)    
            self.renderer.AddActor(gNameActor)
            ind += 1
    
    def addVolumeClipperPlane(self):
        if not self.volumeClipperPlaneAdded:
            self.volumeClipperWidget = vtk.vtkImagePlaneWidget()
            self.volumeClipperWidget.SetInteractor(self.renderInteractor)
            self.planeClip = vtk.vtkPlane()
            self.planeClip.SetOrigin(self.volumeClipperWidget.GetOrigin())
            self.planeClip.SetNormal(self.volumeClipperWidget.GetNormal())
            self.volumeClipperWidget.GetPlaneProperty().SetColor(1, 0 , 0)
            self.volumeClipperWidget.GetTexturePlaneProperty().SetOpacity(0.001)
            self.volumeClipperPlaneAdded = True
        
        self.volumeClipperWidget.SetPlaneOrientationToXAxes()
        self.volumeClipperWidget.SetInput(self.dataImporter.GetOutput())
        self.volumeClipperWidget.Off()
        self.volumeClipperWidget.DisplayTextOn()
        self.volumeMapper.AddClippingPlane(self.planeClip)
        self.volumeClipperWidget.PlaceWidget()
        self.volumeClipperWidget.SetSliceIndex(self.sliceNb)
        self.volumeClipperWidget.SetEnabled(True)
        self.volumeClipperWidget.AddObserver("InteractionEvent", self.volumeClipperWidgetCallback)
        self.renderer.Render() 
    
    def disableVolumeClipperPlane(self):
        if self.volumeClipperPlaneAdded:
            self.volumeClipperWidget.SetEnabled(False)
            
    def enableVolumeClipperPlane(self):
        if self.volumeClipperPlaneAdded:
            self.volumeClipperWidget.SetEnabled(True) 
         
    def volumeClipperWidgetCallback(self, obj, event):
        self.planeClip.SetOrigin(self.volumeClipperWidget.GetOrigin())
        self.planeClip.SetNormal(self.volumeClipperWidget.GetNormal())
        
    def CharEventFunction(self, obj, ev):
        keypressed = self.renderInteractor.GetKeyCode()
        if keypressed == 't':
            self.renderer.RemoveActor(self.logotxt)
            self.renderWin.Render()
        if keypressed == "w":
            self.generatePickleColormap(self.maxLabel)
        if keypressed == "e":
            self.generatePickleColormap(Parameters.newColormapCellNumber)
        if keypressed == "s":    
            pos = np.uint(self.volumeClipperWidget.GetCurrentCursorPosition())
            cid = self.imageCopy[pos[0], pos[1], pos[2]]
            print pos, cid
 
    def droppedOnVTKWindow(self, path):
        self.droppedFile = str(path)
        print self.droppedFile        
        filename, extension = os.path.splitext(self.droppedFile)
        if extension in [".tif", ".tiff"]:
            msg = "Is this a segmented image ? " 
            msg += "Click No if it is an intensity image"
            reply = QtGui.QMessageBox.question(self, 'Message', 
                     msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

            if reply == QtGui.QMessageBox.Yes:                 
                self.ui.segmentationRadioButton.setChecked(True)
            if reply == QtGui.QMessageBox.No:
                self.ui.intensityRadioButton.setChecked(True)
            
            self.dataSpacing = None
            self.readFile(self.droppedFile)
            self.showData()       
        elif extension in [".cmp"]:
            fobj = file(self.droppedFile)
            self.colormap = np.array(cPickle.load(fobj))
            fobj.close()
            self.changeColormap()

        elif extension == ".pat" :
            fobj = file(self.droppedFile)
            self.genePatterns = cPickle.load(fobj)
            fobj.close()
            self.loadPatterns()
    
    def loadPatterns(self):
        L1Patterns = dict()
        if False:
            for g, l in self.genePatterns.iteritems():
                L1Patterns[g] = set(self.genePatterns[g]).intersection(self.genePatterns["L1"])
            self.genePatterns = L1Patterns
        
        self.ui.userNamespace["patterns"] = self.genePatterns
        for gn, gchild in self.ui.GeneChilds.iteritems():
            self.ui.genesItem.removeChild(gchild)
            del gchild
        geneNameL = self.genePatterns.keys()
        geneNameL.sort()
        for geneName in geneNameL:
            cids = self.genePatterns[geneName]
            self.ui.GeneChilds[geneName] = QtGui.QTreeWidgetItem(self.ui.genesItem)
            self.ui.GeneChilds[geneName].setFlags(self.ui.GeneChilds[geneName].flags() | QtCore.Qt.ItemIsUserCheckable)
            self.ui.GeneChilds[geneName].setText(0, geneName)
            self.ui.GeneChilds[geneName].setCheckState(0, QtCore.Qt.Unchecked)
    
if __name__ == "__main__":
    from pkg_resources import resource_filename, Requirement
    
    path = resource_filename("atlasviewer", "")
    pathL = path.split("/")
    path = ""
    for p in pathL[:-1]:
        path += p   
        path += "/"
    path += "data/"
    imFile = path + "test_segmentation_tvformat.tif"
    colorMapFile = path + "colorMap.cmp"

    app = QtGui.QApplication(sys.argv)    
    
    username = "Atlas user"
    useAtlasGlobalColorCodes = False   
    window = Viewer(username, imFile, magnification = 14, colorMapFile = colorMapFile, wallId=0, checkBackground=False, addCellWalls=True, tvOutput = False, useAtlasGlobalColorCodes = useAtlasGlobalColorCodes)
    window.renderInteractor.Initialize() # Need this line to actually show the render inside Qt
    window.show()
    sys.exit(app.exec_())
        
        
    