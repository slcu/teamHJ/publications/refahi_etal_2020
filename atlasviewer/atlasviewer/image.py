############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2012 - 2018, Yassin Refahi
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are not permitted.
#
############################################################################

import os
import pickle
import cPickle
import zipfile
import numpy as np
import scipy.ndimage as nd
from scipy.ndimage.morphology import binary_dilation
from scipy.spatial.distance import cdist
from atlasviewer.avtiff import avimread, avimsave
from matplotlib.pyplot import colormaps

def checkBackgroundImage(image, backgroundLabel = 1):
    """
    Set the background to backgroundLabel.     
    """
    maxLabel = np.max(image)
    largestCellLabel = np.bincount(image.flatten()).argmax()
    if largestCellLabel != backgroundLabel:
        np.putmask(image, image == backgroundLabel, maxLabel + 1)
        maxLabel += 1   
        np.putmask(image, image == largestCellLabel, backgroundLabel)
        return True, image
    else:
        return False, None


def checkBackground(segmentedImageFileName, targetFileName , backgroundLabel = 1):
    """
    Read segmentedImageFileName and set the background to backgroundLabel. 
    Save the result in targetImageFileName 
    """
    image, info = avimread(segmentedImageFileName)
    changed, image = checkBackgroundImage(image, backgroundLabel)
    if changed:
        avimsave(targetFileName, image, voxelWidth = info["voxelWidth"], 
                 voxelHeight = info["voxelHeight"],
                 voxelDepth = info["voxelDepth"], type = "segmented")
    else:
        print """The largest cell has background label. 
        No new file is generated."""
        
    
def rangeCorrespondance_int(a, b, c, d, x):
    print "rangeCorrespondance", (a, b, c, d)
    return ((d - c) / (b - a)) * (x - a) + c

def rangeCorrespondance(a, b, c, d, x):
#     print "rangeCorrespondance", (a, b, c, d)
    return np.array(((float(d) - c) / (b - a)) * (x - a) + c, dtype = x.dtype)


def extractL1L2(image, backgroundLabel, maxLabel= None, checkCellsOnZLast = True):
    """
    Extract cell labels in L1 and L2
    """
    if maxLabel == None:
        maxLabel = np.max(image)
    backgroundLabelVoxels = (image == backgroundLabel)
    backgroundLabelVoxelsDil = binary_dilation(backgroundLabelVoxels, 
                                               iterations = 1)
    externalVoxels = image[backgroundLabelVoxelsDil - backgroundLabelVoxels]
    L1LabelsSet = set(np.unique(externalVoxels))
    if checkCellsOnZLast:
        L1LabelsSet.update( set(np.unique(image[..., -1])) )
    if backgroundLabel in L1LabelsSet:
        L1LabelsSet.remove(backgroundLabel) 
    L1Labels = list(L1LabelsSet)
    L1Characteristic = np.empty((maxLabel + 1, ), bool)
    L1Characteristic.fill(False)
    L1Characteristic[L1Labels] = True 
    L1CellsVoxels = L1Characteristic[image]        
    L2LabelsSet = set(np.unique(image[binary_dilation(L1CellsVoxels, 
                                            iterations = 1) - L1CellsVoxels]))
    L2LabelsSet.remove(backgroundLabel)        
    L2Labels = list(L2LabelsSet)
    return L1Labels, L2Labels


def extractL2GivenL1(image, backgroundLabel, givenL1, maxLabel= None, checkCellsOnZLast = True):
    """
    Extract cell labels in L1 and L2
    """
    if maxLabel == None:
        maxLabel = np.max(image)

    L1Labels = list(givenL1)
    L1Characteristic = np.empty((maxLabel + 1, ), bool)
    L1Characteristic.fill(False)
    L1Characteristic[L1Labels] = True 
    L1CellsVoxels = L1Characteristic[image]        
    L2LabelsSet = set(np.unique(image[binary_dilation(L1CellsVoxels, 
                                            iterations = 1) - L1CellsVoxels]))
    L2LabelsSet.remove(backgroundLabel)        
    L2Labels = list(L2LabelsSet)
    return L1Labels, L2Labels

def applyPalette(im, base_dict, threshold=None, dec=10000):
    outliers = set(np.array(base_dict.keys()) 
                [ np.array(base_dict.values())>threshold] )
    palette=np.zeros(np.max(im)+1, dtype=np.uint16)
    for i in outliers:
        palette[i]=base_dict[i]*dec
    return palette[im] 

def consecutiveLabels(im):
    """
    Return a numpy array whose elements are consecutive starting from 1. 
    """
    if (set(np.unique(im))!=set(range(1, np.max(im)+1))):
        im[im==0]=im.max()+1
        histo = nd.histogram(im, min=0, max=np.max(im), bins=np.max(im)+1)
        if np.argsort(histo)[-1]!=1:
            im[im==1]=im.max()+1
            im[im==np.argsort(histo)[-1]]=1
        labels=np.unique(im)
        labels.sort()
        labels_con=np.linspace(1, len(labels), len(labels)).astype(np.uint16)
        mapping=dict(zip(labels, labels_con))
        return applyPalette(im, mapping, dec=1), zip(labels, labels_con)
    else:
        return im, None




def makeImageLabelsConsecutive(image):
    labels = np.unique(image)
    colorPalette = np.zeros(np.max(image) + 1, dtype = image.dtype)
    counter = 1
    matching = {0:0}
    for lab in labels:
        colorPalette[lab] = counter
        matching[lab] = counter
        counter += 1
    return colorPalette[image], matching
        

def relabel(image):    
    labels = set(np.unique(image))
    image = np.swapaxes(image, 2, 0) ### attention !!!!!!!!!
    labelCounter = 1
    colorPallet = np.zeros(np.max(image) + 1, dtype = np.uint16)
    for k in xrange(image.shape[2]):#-1, -1, -1):
        for i in xrange(image.shape[0]):
            for j in xrange(image.shape[1]):
                if image[i, j, k] in labels:
                    colorPallet[image[i, j, k]] = labelCounter 
                    labelCounter += 1
                    labels.remove(image[i, j, k])
    result = colorPallet[image]
    result = np.swapaxes(result, 0, 2) ### attention !!!!!!!!!
    return result


def computeL1Width(image, backgroundLabel, maxLabel, imageResolution, 
                   checkCellsOnZLast = True):
    """
    Compute L1 layers cells width
    """
    backgroundLabelVoxels = (image == backgroundLabel)
    backgroundLabelVoxelsDil = binary_dilation(backgroundLabelVoxels, 
                                               iterations = 1)
    externalVoxels = image[backgroundLabelVoxelsDil - backgroundLabelVoxels]
    L1LabelsSet = set(np.unique(externalVoxels))
    if checkCellsOnZLast:
        L1LabelsSet.update( set(np.unique(image[..., -1])) )
    L1LabelsSet.remove(backgroundLabel) 
    L1Labels = list(L1LabelsSet)
    L1Characteristic = np.empty((maxLabel + 1, ), bool)
    L1Characteristic.fill(False)
    L1Characteristic[L1Labels] = True 
    L1CellsVoxels = L1Characteristic[image]
    L2ExternalVoxels = image[binary_dilation(L1CellsVoxels, iterations = 1) - 
                             L1CellsVoxels]        
    L2LabelsSet = set(np.unique(L2ExternalVoxels))
    L2LabelsSet.remove(backgroundLabel)        
    L2Labels = list(L2LabelsSet)
    L2Characteristic = np.empty((maxLabel + 1, ), bool)
    L2Characteristic.fill(False)
    L2Characteristic[L2Labels] = True
    L2CellsVoxels = L2Characteristic[image]
    L1BottomLayer = binary_dilation(L2CellsVoxels, iterations = 1) & \
                    L1CellsVoxels
    L1TopLayer = backgroundLabelVoxelsDil & L1CellsVoxels
    L1New = np.unique(image[L1BottomLayer])
    newImage = np.ones(image.shape, dtype = image.dtype)
    newImage[L1BottomLayer] = image[L1BottomLayer]
    newImage[L1TopLayer] = image[L1TopLayer]
    wTop = np.where(L1TopLayer) 
    wBottom = np.where(L1BottomLayer)
#    resolution = getTiffFileResolution(intensityFName)
    resolution = imageResolution
    cellsL1Width = {}
    L1WidthArray = np.zeros(maxLabel + 1)
    counter = 0
    for cid in L1LabelsSet:
        print float(counter) / len(L1LabelsSet)
        wcid = (image == cid)
        wTop = np.where(L1TopLayer & wcid) 
        wBottom = np.where(L1BottomLayer & wcid)
        positionsTop = np.array(zip(wTop[0] * resolution[0], 
                            wTop[1] * resolution[1], wTop[2] * resolution[2]))
        positionsBottom = np.array(zip(wBottom[0] * resolution[0], 
                    wBottom[1] * resolution[1], wBottom[2] * resolution[2]))
        if positionsTop.shape[0] > 0 and positionsBottom.shape[0] > 0:
            distances = cdist(positionsTop, positionsBottom)
            layerAverage = np.average(np.min(distances, axis = 0))
            print cid, "average = ", layerAverage
            L1WidthArray[cid] = layerAverage
        else:
            print cid, "average = 0"
            layerAverage = 0
        cellsL1Width[cid] = layerAverage
        counter += 1
    return L1Labels, L2Labels, L1New, cellsL1Width, L1WidthArray 
    

def checkBG(fName, background = 1):
    im, tags = tiffread(fName)
    maxLabel = np.max(im)
    print "Checking the background label ..."
    largestCellLabel = np.bincount(im.flatten()).argmax()
    if largestCellLabel != background:
        np.putmask(im, im == background, maxLabel + 1)
        maxLabel += 1   
        np.putmask(im, im == largestCellLabel, background)
    tiffsave(im, fName, **tags)


def generateNextColormap(currentColormapFName, matchingScoresFName, 
                         tFirstToRemove = [], background = 1):
    fobj = file(matchingScoresFName)
    (matching, scoresList) = cPickle.load(fobj)
    fobj.close()
    fobj = file(currentColormapFName)
    cmap = cPickle.load(fobj)
    fobj.close()
    divisions = dict()
    maxDaughterLabel = -1
    for (d, m) in matching:
        divisions.setdefault(m, []).append(d)
        maxDaughterLabel = max(maxDaughterLabel, d)
    counter = 0
    for m, dList in divisions.iteritems():
        if len(dList) > 1:
            counter += 1
    newColormap = np.ones(cmap.shape, dtype = cmap.dtype)
    newColormap[0] = 0
#     newColormap = np.array(cmap)
    for m, dList in divisions.iteritems():
        if m in tFirstToRemove:
            for d in dList:
                newColormap[d] = background
        else:
            for d in dList:
                newColormap[d] = cmap[m]       
    return newColormap


def generateGeneColormap(currentColormapFName, geneColorCodes, 
                         tFirstToRemove = [], background = 1):

    fobj = file(currentColormapFName)
    cmap = cPickle.load(fobj)
    fobj.close()
    
    newColormap = np.ones(cmap.shape, dtype = cmap.dtype)
    newColormap[0] = 0
#     newColormap = np.array(cmap)
    for cid in xrange(len(geneColorCodes)):
        newColormap[cid] = geneColorCodes[cid]
        
    fobj = file(currentColormapFName[:-4] + "_test.cmp", "w")
    cPickle.dump(newColormap, fobj)
    fobj.close()       
    return newColormap




def extractL1L2Width(fName, backgroundLabel = 1,  checkCellsOnZLast = True, SF = 1):
    image, tags = tiffread(fName)
    image = np.swapaxes(image, 2, 0)
    maxLabel = np.max(image)
    resolution = getTiffFileResolution(fName)
    resolution = resolution[0], resolution[1], resolution[2] / SF
        
    backgroundLabelVoxels = (image == backgroundLabel)
    backgroundLabelVoxelsDil = binary_dilation(backgroundLabelVoxels, iterations = 1)
    externalVoxels = image[backgroundLabelVoxelsDil - backgroundLabelVoxels]
    L1LabelsSet = set(np.unique(externalVoxels))
    if checkCellsOnZLast:
        L1LabelsSet.update( set(np.unique(image[..., -1])) )
    L1LabelsSet.remove(backgroundLabel) 
    L1Labels = list(L1LabelsSet)
    L1Characteristic = np.empty((maxLabel + 1, ), bool)
    L1Characteristic.fill(False)
    L1Characteristic[L1Labels] = True 
    L1CellsVoxels = L1Characteristic[image]
    L2ExternalVoxels = image[binary_dilation(L1CellsVoxels, iterations = 1) - L1CellsVoxels]        
    L2LabelsSet = set(np.unique(L2ExternalVoxels))
    L2LabelsSet.remove(backgroundLabel)        
    L2Labels = list(L2LabelsSet)
    L2Characteristic = np.empty((maxLabel + 1, ), bool)
    L2Characteristic.fill(False)
    L2Characteristic[L2Labels] = True
    L2CellsVoxels = L2Characteristic[image]
    
    L1BottomLayer = binary_dilation(L2CellsVoxels, iterations = 1) & L1CellsVoxels
    L1TopLayer = backgroundLabelVoxelsDil & L1CellsVoxels
    L1New = np.unique(image[L1BottomLayer])
    newImage = np.ones(image.shape, dtype = image.dtype)
    newImage[L1BottomLayer] = image[L1BottomLayer]
    newImage[L1TopLayer] = image[L1TopLayer]
       
    print len(np.where(L1BottomLayer == True)[0]), len(np.where(L1TopLayer == True)[0])
#     newImage[externalVoxels] = image[externalVoxels]
#     externalVoxels[L1BottomLayer] = image[L1BottomLayer]
#     imsave(segFName[:-4] + "_test.tif", SpatialImage(newImage))
    
    wTop = np.where(L1TopLayer) 
    wBottom = np.where(L1BottomLayer)

    print resolution
    resolution = [resolution[0], resolution[1], resolution[2] /1.3]
    cellsL1Width = {}
    L1WidthArray = np.zeros(maxLabel + 1)
    counter = 0
    for cid in L1LabelsSet:
        print float(counter) / len(L1LabelsSet)
        wcid = (image == cid)
        wTop = np.where(L1TopLayer & wcid) 
        wBottom = np.where(L1BottomLayer & wcid)
        positionsTop = np.array(zip(wTop[0] * resolution[0], wTop[1] * resolution[1], wTop[2] * resolution[2]))
        positionsBottom = np.array(zip(wBottom[0] * resolution[0], wBottom[1] * resolution[1], wBottom[2] * resolution[2]))
#         print cid, positionsTop.shape, positionsBottom.shape
        if positionsTop.shape[0] > 0 and positionsBottom.shape[0] > 0:
            distances = cdist(positionsTop, positionsBottom)
            layerAverage = np.average(np.min(distances, axis = 0))
            print cid, "average = ", layerAverage
            L1WidthArray[cid] = layerAverage
        else:
            print cid, "average = 0"
            layerAverage = 0
        cellsL1Width[cid] = layerAverage
        counter += 1
        
    return L1Labels, L2Labels, L1New, cellsL1Width, L1WidthArray 




def boundaryCells(image):
    bCells = set()
    bCells.update(np.unique(image[0, :, :]))
    bCells.update(np.unique(image[-1, :, :]))
    bCells.update(np.unique(image[:, 0, :]))
    bCells.update(np.unique(image[:, -1, :]))
    bCells.update(np.unique(image[:, :, 0]))
    bCells.update(np.unique(image[:, :, -1]))
    return bCells



def makepatternCellsValues(patterns, selectedGenes, minCellsNbInpattern = 0):
    "the same as makepatternCellsValuesVerbose, the only differenceis that it is not verbose" 
    patterns = dict(patterns)
    genesNames = patterns.keys()
    for gn in genesNames:
        if gn not in selectedGenes:
            del patterns[gn] 
    allCIds = set()
    for gene, labels in patterns.iteritems():
        allCIds.update(labels)
    cidGenes = dict((i, set()) for i in allCIds)
    for gene, labels in patterns.iteritems():
        for i in labels:
            cidGenes[i].add(gene)
    allTags = set()
    for i, genes in cidGenes.iteritems():
        allTags.add(frozenset(genes))
    
    tagsCIds = dict((t, set()) for t in allTags)
    for i, genes in cidGenes.iteritems():
        tagsCIds[frozenset(genes)].add(i)
    counter = 0
    tagsColomapId = {}
    colormapId = 2
    
    for tags, cidList in tagsCIds.iteritems():
        if len(cidList) > 0:
            counter += 1
            tagsColomapId[tags] = colormapId
            colormapId += 1
        elif len(tags) == 1:
            print "here: ", tags
    z = zip(tagsColomapId.keys(), tagsColomapId.values())
    z.sort()

    tagsCMapIdNew = dict(tagsColomapId)
    tCIdsNew = dict(tagsCIds)
    
    tagsToReplace = set()
    tagsCellNbs = [(t, len(cL)) for t, cL in tCIdsNew.iteritems()]
    for item in tagsCellNbs:
        if item[1] < minCellsNbInpattern:
            tagsToReplace.add(item[0])
    
    for t in tagsToReplace:
        nearestT = findNearestTag(t, tCIdsNew)
        
        tCIdsNew[nearestT[-1]].update(tCIdsNew[t])
        del tCIdsNew[t]
        del tagsCMapIdNew[t]
    cellsValuesDict = {}
    tagsCMapIdNewOrdered = {}
    
    counter = 2
    for t, code in tagsCMapIdNew.iteritems():
        tagsCMapIdNewOrdered[t] = counter
        counter += 1
    for t, cL in tCIdsNew.iteritems():
        for cid in cL:
            cellsValuesDict[cid] = tagsCMapIdNewOrdered[t]
        
    return cellsValuesDict, tagsCMapIdNewOrdered, tCIdsNew





def makepatternCellsValuesNew(patterns, selectedGenes, imposedPatterns = None):
    patterns = dict(patterns)
    genesNames = patterns.keys()
    for gn in genesNames:
        if gn not in selectedGenes:
            del patterns[gn] 
    allCIds = set()
    for gene, labels in patterns.iteritems():
        allCIds.update(labels)
    cidGenes = dict((i, set()) for i in allCIds)
    for gene, labels in patterns.iteritems():
        for i in labels:
            cidGenes[i].add(gene)
    allTags = set()
    for i, genes in cidGenes.iteritems():
        allTags.add(frozenset(genes))
    
    tagsCIds = dict((t, set()) for t in allTags)
    for i, genes in cidGenes.iteritems():
        tagsCIds[frozenset(genes)].add(i)
    if imposedPatterns is None:
        colormapId = 2
        imposedPatterns = {}
        tagsColomapId = {}
    else:
        colormapId = max(imposedPatterns.values()) + 1
        tagsColomapId = dict(imposedPatterns) 
    
    print "colormapId = ", colormapId
    
    cellsValuesDict = {}
    tagsCMapIdNewOrdered = tagsColomapId
#     counter = 2
#     for t, code in tagsColomapId.iteritems():
#         tagsCMapIdNewOrdered[t] = counter
#         counter += 1
    for t, cL in tagsCIds.iteritems():
        for cid in cL:
            cellsValuesDict[cid] = tagsCMapIdNewOrdered[t]
        
    
    return cellsValuesDict, tagsCMapIdNewOrdered, tagsCIds



























  
