############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# * Redistribution and use in source and binary forms, with or without
# * modification, are not permitted.
#
############################################################################



from setuptools import setup, find_packages
from codecs import open
from os import path


currentPath = path.abspath(path.dirname(__file__))

with open(path.join(currentPath, 'DESCRIPTION.rst'), encoding='utf-8') as f:
    long_description = f.read()


setup(name='atlasviewer',
      version='0.9.1.dev',
      description='A package to visualize 3D data',
      long_description=long_description,
      url='',
      author='Yassin Refahi',
      author_email='yassin.refahi@inrae.fr',
      license='',
      classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Modellers',
        'Topic :: Image Processing',

        # Pick your license as you wish (should match "license" above)
        'License :: ',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.        
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',               
    ],
      data_files = [('data', ['data/test_segmentation_tvformat.tif', 'data/colorMap.cmp', 'data/test_intensity_image_tvformat.tif'])],
      packages = find_packages(exclude=['contrib', 'docs', 'tests*']),
      keywords='Image Processing',
      install_requires = ["numpy", "scipy", "libtiff", "matplotlib", "vtk"],
      zip_safe=False)
