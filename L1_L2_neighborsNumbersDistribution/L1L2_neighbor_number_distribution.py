import cPickle
import numpy as np
from atlasviewer.mesh import TissueAnalysis
import matplotlib.pyplot as plt
from atlasviewer.avtiff import avimread, voxelDimensionsFromTiffFile
from fileNames import atlas_pattern_fileNames_L1, atlas_pattern_fileNames_L2, atlas_pattern_fileNames_L1L2, atlas_pattern_fileNames_all, YR_01_segmentations_file_names


__doc__ = """
Plot L1-L1, L1-L2, L2-L1, L2-L2 neighbor number distributions.
"""


def getCidsFromPattern(fName, geneName):
    fobj = file(fName)
    data = cPickle.load(fobj)
    fobj.close()
    return set(data[geneName])



def L1_L2_neighbor_numbers(segmentationFName, patternFileName):
    L1CellIds = getCidsFromPattern(patternFileName, "L1")
    L2CellIds = getCidsFromPattern(patternFileName, "L2")
    image, info= avimread(segmentationFName)
    image = np.swapaxes(image, 2, 0)
    resolution = voxelDimensionsFromTiffFile(segmentationFName)
    tA = TissueAnalysis(image, resolution, removeBoundaryCells=False)
    tA.makeGraph()
    
    L1_L1 = []
    L1_L2 = []
    L2_L1 = []
    L2_L2 = []
    
    for c, N in tA.cellsNeighbors.iteritems():
        if c in L1CellIds:
            c_L1_L1Neigbors = [nid for nid in N if nid in L1CellIds]
            L1_L1.append(len(c_L1_L1Neigbors))
            if len(c_L1_L1Neigbors) == 0:
                print c, 
            
            c_L1_L2Neigbors = [nid for nid in N if nid in L2CellIds]
            L1_L2.append(len(c_L1_L2Neigbors))
            
            if len(c_L1_L2Neigbors) == 0:
                print c, 
            
        if c in L2CellIds:
            c_L2_L2Neigbors = [nid for nid in N if nid in L2CellIds]
            L2_L2.append(len(c_L2_L2Neigbors))
            
            if len(c_L2_L2Neigbors) == 0:
                print c,
            
            c_L2_L1Neigbors = [nid for nid in N if nid in L1CellIds]
            L2_L1.append(len(c_L2_L1Neigbors))
            
            if len(c_L2_L1Neigbors) == 0:
                print c,
        
    return L1_L1, L1_L2, L2_L1, L2_L2

L1_L1 = []
L1_L2 = []
L2_L1 = []
L2_L2 = []

segFName = YR_01_segmentations_file_names["10h"]
patFName = atlas_pattern_fileNames_all["10h"]
L1_L1_10h, L1_L2_10h, L2_L1_10h, L2_L2_10h = L1_L2_neighbor_numbers(segFName, patFName)
print "extracted"    
L1_L1.extend(L1_L1_10h)
L1_L2.extend(L1_L2_10h)
L2_L1.extend(L2_L1_10h)
L2_L2.extend(L2_L2_10h)

 
 
segFName = YR_01_segmentations_file_names["40h"]
patFName = atlas_pattern_fileNames_all["40h"]
L1_L1_40h, L1_L2_40h, L2_L1_40h, L2_L2_40h= L1_L2_neighbor_numbers(segFName, patFName)
print "extracted"        
L1_L1.extend(L1_L1_40h)
L1_L2.extend(L1_L2_40h)
L2_L1.extend(L2_L1_40h)
L2_L2.extend(L2_L2_40h)


 
segFName = YR_01_segmentations_file_names["96h"]
patFName = atlas_pattern_fileNames_all["96h"]
L1_L1_96h, L1_L2_96h, L2_L1_96h, L2_L2_96h= L1_L2_neighbor_numbers(segFName, patFName)    
L1_L1.extend(L1_L1_96h)
L1_L2.extend(L1_L2_96h)
L2_L1.extend(L2_L1_96h)
L2_L2.extend(L2_L2_96h)
print "extracted"    
 

segFName = YR_01_segmentations_file_names["120h"]
patFName = atlas_pattern_fileNames_all["120h"]
L1_L1_120h, L1_L2_120h, L2_L1_120h, L2_L2_120h= L1_L2_neighbor_numbers(segFName, patFName)    
L1_L1.extend(L1_L1_120h)
L1_L2.extend(L1_L2_120h)
L2_L1.extend(L2_L1_120h)
L2_L2.extend(L2_L2_120h)
print "extracted"    
 
 
segFName = YR_01_segmentations_file_names["132h"]
patFName = atlas_pattern_fileNames_all["132h"]
L1_L1_132h, L1_L2_132h, L2_L1_132h, L2_L2_132h = L1_L2_neighbor_numbers(segFName, patFName)
print "extracted"        
L1_L1.extend(L1_L1_132h)
L1_L2.extend(L1_L2_132h)
L2_L1.extend(L2_L1_132h)
L2_L2.extend(L2_L2_132h)


        
print L1_L1.count(0), L1_L2.count(0), L2_L1.count(0), L2_L2.count(0) 
    
plotL1_L1 = True        
if plotL1_L1:
    b = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5]
    t = plt.hist(L1_L1, bins = b)
    plt.title("Number of L1 neighbors in L1, n = %d"%len(L1_L1))
    plt.grid(True)
    plt.ylabel("Frequency")
    plt.show()
    

plotL1_L2 = False        
if plotL1_L2:
    b = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5]
    t = plt.hist(L1_L2, bins = b)
    plt.title("Number of L1 neighbors in L2, n = %d"%len(L1_L2))
    plt.grid(True)
    plt.ylabel("Frequency")
    plt.show()
    

plotL2_L1 = False        
if plotL2_L1:
    b = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5]
    t = plt.hist(L2_L1, bins = b)
    plt.title("Number of L2 neighbors in L1, n = %d"%len(L2_L1))
    plt.grid(True)
    plt.ylabel("Frequency")
    plt.show()
    

plotL2_L2 = False        
if plotL2_L2:
    b = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5]
    t = plt.hist(L2_L2, bins = b)
    plt.title("Number of L2 neighbors in L2, n = %d"%len(L2_L2))
    plt.grid(True)
    plt.ylabel("Frequency")
    plt.show()
    



