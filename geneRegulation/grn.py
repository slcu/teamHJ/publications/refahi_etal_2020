class Act(object):
    def __init__(self):
        pass

    def __repr__(self):
        return "-->"

class Repr(object):
    def __init__(self):
        pass

    def __repr__(self):
        return "--|"

class Interaction(object):
    def __init__(self, src, dest, op):
        self.src = src
        self.dest = dest
        self.op = op

    def __repr__(self):
        return (self.src +
                " " + repr(self.op) + " "
                + self.dest)

    def __hash__(self):
        return hash((self.src, repr(self.op), self.dest))

    def __eq__(self, other):
        return (self.src == other.src and
                self.dest == other.dest and
                repr(self.op) == repr(other.op))

    def isAct(self):
        return repr(self.op) == "-->"

    def isRepr(self):
        return repr(self.op) == "--|"

    def rename(self, g, ng):
        if self.src == g: self.src = ng
        if self.dest == g: self.dest = ng

        return

    def isLoop(self):
        return self.src == self.dest

    
class GRN(object):

    def __init__(self, fn):
        self.interactions = self.parseGRN(fn)
        return

    def _parseAct(self, ln):
        genes = ln.split("-->")
        if len(genes) == 2:
            src = genes[0].strip()
            dest = genes[1].strip()
            return Interaction(src, dest, Act())
        else:
            return None

    def _parseRepr(self, ln):
        genes = ln.split("--|")
        if len(genes) == 2:
            src = genes[0].strip()
            dest = genes[1].strip()
            return Interaction(src, dest, Repr())
        else:
            return None
        
    def _parseInteraction(self, ln):
        actIntr = self._parseAct(ln)
        reprIntr = self._parseRepr(ln)
        
        if actIntr:
            return actIntr
        elif reprIntr:
            return reprIntr
        else:
            raise Exception("parse error")
        
    def parseGRN(self, fn):
        interactions = []
        with open(fn, 'r') as f:
            for line in f:
                intr = self._parseInteraction(line)
                interactions.append(intr)

        return interactions

    def toPairs(self):
        return [(intr.src, intr.dest) for intr in self.interactions]
    

    def getInteractions(self, geneNm):
        return [intr for intr in self.interactions if geneNm in intr.dest]

    def filterInteractions(self, geneNms):
        self.interactions =  [intr for intr in self.interactions
                              if intr.src in geneNms and intr.dest in geneNms]
        return

    def noDups(self):
        self.interactions = list(set(self.interactions))

    def renameGene(self, g, ng):
        for intr in self.interactions:
            intr.rename(g, ng)

    def diff(self, other):
        return list(set.difference(self.interactions, other.interactions))

    def getUniqGeneNames(self):
        from functools import reduce
        
        genePairs = [[intr.src, intr.dest] for intr in self.interactions]

        return set(reduce(lambda x, y:x+y, genePairs))


def mkDegr():
    degrRepr = "degradationOne 1 0"
    degrRepr += "\n"
    degrRepr += "0.1"

    return degrRepr
    
def mkHill(idxs, interactions):
    #interactions with same dest
    nActs = sum([1 for intr in interactions if intr.isAct()])
    nReprs = sum([1 for intr in interactions if intr.isRepr()])
    vMax = 0.1
    K = 0.5
    n = 2
    nParams = (nActs + nReprs)*2 + 1
    nLevels = 2
    
    hillRepr = " ".join(["hill", str(nParams), str(nLevels), str(nActs), str(nReprs)])
    hillRepr += "\n"
    hillRepr += str(vMax)
    hillRepr += "\n"
    
    for i in range(len(interactions)):
        hillRepr += "\n".join([str(K), str(n)])
        hillRepr += "\n"

    
    for intr in interactions:
        hillRepr += str(idxs[intr.src])
        hillRepr += ("   #" + repr(intr))
        hillRepr += "\n"
        
    return hillRepr

def mkNet(geneNms, gn):
    sgenes = set(geneNms)
    idxs = dict([(g, i+5) for i, g in enumerate(geneNms)])
    netRepr = ""
    
    for g in geneNms:
        gIntrs = [gi for gi in gn.getInteractions(g) if gi.src in sgenes]
        gIntrsF = []
        for gi in gIntrs:
            if gi.isAct(): gIntrsF.append((gi, 1))
            else: gIntrsF.append((gi, 2))

        gIntrsSorted = map(lambda x: x[0], sorted(gIntrsF, key=lambda x: x[1]))
        
        if not gIntrsSorted:
            netRepr += " ".join([g, str(idxs[g]), "0"])
        else:
            netRepr += " ".join([g, str(idxs[g]), "2"])
            netRepr += "\n"
            netRepr += mkHill(idxs, gIntrsSorted)
            netRepr += "\n"
            netRepr += mkDegr()

        netRepr += "\n"
        netRepr += "\n"
        
    return netRepr


def getInteractionsGenes(geneNms, gn):
    sgenes = set(geneNms)

    allIntrs = []
    
    for g in geneNms:
        inputs = tuple([gi.src for gi in gn.getInteractions(g) if gi.src in sgenes])
        allIntrs.append((inputs, g))

    return allIntrs

def getInteractionsGenesT(geneNms, gn):
    sgenes = set(geneNms)

    allIntrs = []

    for g in geneNms:
        acts = tuple([gi.src for gi in gn.getInteractions(g) if gi.src in sgenes and gi.isAct()])
        reprs = tuple([gi.src for gi in gn.getInteractions(g) if gi.src in sgenes and gi.isRepr()])

        allIntrs.append((acts, reprs, g))

    return allIntrs
        
