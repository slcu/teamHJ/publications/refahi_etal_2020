import common.lin as lin
import grn
import boolFs as b
import _pickle as cPickle


def mk_fs():
    tss, linss = lin.mkSeries1(d="../data/FM1/tv/",
                               dExprs="../data/geneExpression/",
                               linDataLoc="../data/FM1/tracking_data/",
                               ft=lambda t: t in {10, 40, 96, 120, 132})
    lin.filterL1_st(tss)
    G = grn.GRN("refInteractions.txt")

    geneNms = G.getUniqGeneNames()

    intrs = grn.getInteractionsGenesT(geneNms, G)

    fsT = b.get_fsT(tss, intrs)
    fsT_D1 = b.get_fsTD1(tss, intrs)

    return fsT, fsT_D1


def write(fsT, fname):
    resT = {}
    for t in [10, 40, 96, 120, 132]:
        resT[t] = b.map_gfs(fsT[t], b.serialise_f)

    with open(fname, "wb") as fout:
        cPickle.dump(resT, fout)


def go():
    fsT, fsT_D1 = mk_fs()
    write(fsT, "fsT.pkl")
    write(fsT_D1, "fsT_D1.pkl")


if __name__ == "__main__":
    go()
