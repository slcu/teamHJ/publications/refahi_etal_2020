## Introduction
This directory contains all the code and data needed to recreate the analysis of the Gene Regulatory Network (GRN) in Figure 1 using
spatial genetic information in the Atlas.

## Scripts and Methods
This part of the analysis code uses Python 3 (any version < 3.9 should work).
To setup a local Python virtual environment:
```
python3 -m venv atlas-env
source atlas-env/bin/activate
```

Install requirements inside this environment:
```
pip install --upgrade pip
pip install -r ../requirements.txt
pip install notebook
pip install ../common/
```

To run the code that analyses the GRN and produces new hypotheses using spatial genetic information as well
as creating and testing random networks used as a baseline do the following:
```
python mk_fs.py
jupyter nbconvert --to notebook --execute random_net.ipynb
jupyter nbconvert --to notebook --execute fig6.ipynb
```

If you just want the subsequent analysis code that generates the figures in the paper, 
```
jupyter nbconvert --to notebook --execute fig6.ipynb

```
in which case the precomputed results available in this directory (.pkl files) will be used instead.

## Main Contacts
Argyris Zardilis argyris.zardilis@slcu.cam.ac.uk
