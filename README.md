## Introduction

Scripts and software used to produce the results in the manuscript "A multiscale analysis of early flower development in Arabidopsis provides an integrated view of molecular regulation and growth control"

## Data
The data supporting the publication are available here:
https://doi.org/10.17863/CAM.61991

Unzip the files in the *data* directory to follow the analyses code in the sub-folders.

## Scripts and software

We have collected the tools to produce the results in folders following the
outline of the paper. For an overview of the methods used, see the Methods section of the paper.

*data*  
Textual representations of the geometries and gene expression patterns for the analysed flower meristems that we used internally for convenience.

*common*   
Common modules used throughout the analysis for handling segmentation files, lineages and input/output of the data.

*atlasviewer*  
An internal tool used for visualisation and handling of meristem segmented images and associated data (e.g. gene expression, volumes).</p>

*estimateVoxelSize*  
Code to calculate corrections to the size of the voxels of the acquired images.

*stateAnalysis*  
Code to perform analysis of the gene configurations (cell states) present in the gene expression patterns and compute the clustering and transition graphs of Figure 3.

*L1_L2_neighborsNumbersDistribution*  
Code to make calculation on neighbour distribution in L1, L2

*geneRegulation*  
Scripts to perform model prediction and hypothesis testing of the gene regulatory network (Figure 4).

*growthAnalysis*  
Scripts for analysing growth and anisotropy changes within the dataset, for example analysing growth patterns over time (Figure 5), correlation of genepatterns to growth patterns (Figure 6), and analysis of growth differences in the lfy mutant (Figure 7).

We have used outside tools, in-house tools, and python scripts. See further details 
in the README files within the specific folders.

## Contact

yassin.refahi@inrae.fr

argyris.zardilis@slcu.cam.ac.uk

henrik.jonsson@slcu.cam.ac.uk

jan.traas@ens-lyon.fr
