import cPickle
import matplotlib.pyplot as plt
import numpy as np
from atlasviewer.image import makepatternCellsValues
from fileNames import YR_01_path
from allTagsDModule import allTagsD_L1_new, allTagsD_L2_new, tagsCodeTuplesL1
from createTagsD import patterns10hOnlyL1, patterns10hOnlyL2, patterns10hAllTissue
from createTagsD import patterns40hOnlyL1, patterns40hOnlyL2, patterns40hAllTissue
from createTagsD import patterns96hOnlyL1, patterns96hOnlyL2, patterns96hAllTissue
from createTagsD import patterns120hOnlyL1, patterns120hOnlyL2, patterns120hAllTissue
from createTagsD import patterns132hOnlyL1, patterns132hOnlyL2, patterns132hAllTissue


fobj = file("FM1_dtissue.tis")
dtis = cPickle.load(fobj)
fobj.close()


appendZero = True


def cidToTag(cid, tagsCIdL):
    for tag, cidL in tagsCIdL.iteritems():
        if cid in cidL:
            return tag
    return None

pat10hKeys = set(patterns10hAllTissue.keys()) - set(["L1", "L2"])
pat40hKeys = set(patterns40hAllTissue.keys()) - set(["L1", "L2"])
pat96hKeys = set(patterns96hAllTissue.keys()) - set(["L1", "L2"])
pat120hKeys = set(patterns120hAllTissue.keys()) - set(["L1", "L2"])
pat132hKeys = set(patterns132hAllTissue.keys()) - set(["L1", "L2"])


pat10hKeysL1 = set(patterns10hOnlyL1.keys()) - set(["L1", "L2"])
pat40hKeysL1 = set(patterns40hOnlyL1.keys()) - set(["L1", "L2"])
pat96hKeysL1 = set(patterns96hOnlyL1.keys()) - set(["L1", "L2"])
pat120hKeysL1 = set(patterns120hOnlyL1.keys()) - set(["L1", "L2"])
pat132hKeysL1 = set(patterns132hOnlyL1.keys()) - set(["L1", "L2"])


pat10hKeysL2 = set(patterns10hOnlyL2.keys()) - set(["L1", "L2"])
pat40hKeysL2 = set(patterns40hOnlyL2.keys()) - set(["L1", "L2"])
pat96hKeysL2 = set(patterns96hOnlyL2.keys()) - set(["L1", "L2"])
pat120hKeysL2 = set(patterns120hOnlyL2.keys()) - set(["L1", "L2"])
pat132hKeysL2 = set(patterns132hOnlyL2.keys()) - set(["L1", "L2"])


cellsValues10h_L1, tagsColomapId10h_L1, tagsCIds10h_L1 = makepatternCellsValues(patterns10hOnlyL1, pat10hKeysL1)
cellsValues40h_L1, tagsColomapId40h_L1, tagsCIds40h_L1 = makepatternCellsValues(patterns40hOnlyL1, pat40hKeysL1)
cellsValues96h_L1, tagsColomapId96h_L1, tagsCIds96h_L1 = makepatternCellsValues(patterns96hOnlyL1, pat96hKeysL1)
cellsValues120h_L1, tagsColomapId120h_L1, tagsCIds120h_L1 = makepatternCellsValues(patterns120hOnlyL1, pat120hKeysL1)
cellsValues132h_L1, tagsColomapId132h_L1, tagsCIds132h_L1 = makepatternCellsValues(patterns132hOnlyL1, pat132hKeysL1)


cellsValues10h_L2, tagsColomapId10h_L2, tagsCIds10h_L2 = makepatternCellsValues(patterns10hOnlyL2, pat10hKeysL2)
cellsValues40h_L2, tagsColomapId40h_L2, tagsCIds40h_L2 = makepatternCellsValues(patterns40hOnlyL2, pat40hKeysL2)
cellsValues96h_L2, tagsColomapId96h_L2, tagsCIds96h_L2 = makepatternCellsValues(patterns96hOnlyL2, pat96hKeysL2)
cellsValues120h_L2, tagsColomapId120h_L2, tagsCIds120h_L2 = makepatternCellsValues(patterns120hOnlyL2, pat120hKeysL2)
cellsValues132h_L2, tagsColomapId132h_L2, tagsCIds132h_L2 = makepatternCellsValues(patterns132hOnlyL2, pat132hKeysL2)


allTagsL1 = set()
allTagsL1.update(tagsColomapId10h_L1.keys())
allTagsL1.update(tagsColomapId40h_L1.keys())
allTagsL1.update(tagsColomapId96h_L1.keys())
allTagsL1.update(tagsColomapId120h_L1.keys())
allTagsL1.update(tagsColomapId132h_L1.keys())
allTagsL1 = list(allTagsL1)
allTagsL1.sort()

allTagsL2 = set()
allTagsL2.update(tagsColomapId10h_L2.keys())
allTagsL2.update(tagsColomapId40h_L2.keys())
allTagsL2.update(tagsColomapId96h_L2.keys())
allTagsL2.update(tagsColomapId120h_L2.keys())
allTagsL2.update(tagsColomapId132h_L2.keys())
allTagsL2 = list(allTagsL2)
allTagsL2.sort()


pattToRemove = []


start = "10h"
stop = "40h"
startPattern = tagsCIds10h_L1
nextPattern = tagsCIds40h_L1


ratioPaire = []

dist_10h_40h_L1 = np.zeros((max(allTagsD_L1_new.values()) + 1, max(allTagsD_L1_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L1_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_10h_40h_L1[allTagsD_L1_new[pat], dP] = patD.count(dP) / float(len(patD))
            

# fobj = file("transitionGraph%s_%s_L1.pkl"%(start, stop), "w")
# cPickle.dump(dist_10h_40h_L1, fobj)
# fobj.close()


start = "40h"
stop = "96h"
startPattern = tagsCIds40h_L1
nextPattern = tagsCIds96h_L1

ratioPaire = []

dist_40h_96h_L1 = np.zeros((max(allTagsD_L1_new.values()) + 1, max(allTagsD_L1_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L1_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_40h_96h_L1[allTagsD_L1_new[pat], dP] = patD.count(dP) / float(len(patD)) 

# fobj = file("transitionGraph%s_%s_L1.pkl"%(start, stop), "w")
# cPickle.dump(dist_40h_96h_L1, fobj)
# fobj.close()

start = "96h"
stop = "120h"
startPattern = tagsCIds96h_L1
nextPattern = tagsCIds120h_L1

ratioPaire = []

dist_96h_120h_L1 = np.zeros((max(allTagsD_L1_new.values()) + 1, max(allTagsD_L1_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L1_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_96h_120h_L1[allTagsD_L1_new[pat], dP] = patD.count(dP) / float(len(patD)) 

# fobj = file("transitionGraph%s_%s_L1.pkl"%(start, stop), "w")
# cPickle.dump(dist_96h_120h_L1, fobj)
# fobj.close()


start = "120h"
stop = "132h"
startPattern = tagsCIds120h_L1
nextPattern = tagsCIds132h_L1

ratioPaire = []

dist_120h_132h_L1 = np.zeros((max(allTagsD_L1_new.values()) + 1, max(allTagsD_L1_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L1_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_120h_132h_L1[allTagsD_L1_new[pat], dP] = patD.count(dP) / float(len(patD)) 

# fobj = file("transitionGraph%s_%s_L1.pkl"%(start, stop), "w")
# cPickle.dump(dist_120h_132h_L1, fobj)
# fobj.close()




start = "10h"
stop = "40h"
startPattern = tagsCIds10h_L2
nextPattern = tagsCIds40h_L2

ratioPaire = []

dist_10h_40h_L2 = np.zeros((max(allTagsD_L2_new.values()) + 1, max(allTagsD_L2_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L2_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_10h_40h_L2[allTagsD_L2_new[pat], dP] = patD.count(dP) / float(len(patD)) 

# fobj = file("transitionGraph%s_%s_L2.pkl"%(start, stop), "w")
# cPickle.dump(dist_10h_40h_L2, fobj)
# fobj.close()


start = "40h"
stop = "96h"
startPattern = tagsCIds40h_L2
nextPattern = tagsCIds96h_L2

ratioPaire = []

dist_40h_96h_L2 = np.zeros((max(allTagsD_L2_new.values()) + 1, max(allTagsD_L2_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L2_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_40h_96h_L2[allTagsD_L2_new[pat], dP] = patD.count(dP) / float(len(patD)) 

# fobj = file("transitionGraph%s_%s_L2.pkl"%(start, stop), "w")
# cPickle.dump(dist_40h_96h_L2, fobj)
# fobj.close()

start = "96h"
stop = "120h"
startPattern = tagsCIds96h_L2
nextPattern = tagsCIds120h_L2

ratioPaire = []

dist_96h_120h_L2 = np.zeros((max(allTagsD_L2_new.values()) + 1, max(allTagsD_L2_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L2_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_96h_120h_L2[allTagsD_L2_new[pat], dP] = patD.count(dP) / float(len(patD)) 

# fobj = file("transitionGraph%s_%s_L2.pkl"%(start, stop), "w")
# cPickle.dump(dist_96h_120h_L2, fobj)
# fobj.close()

start = "120h"
stop = "132h"
startPattern = tagsCIds120h_L2
nextPattern = tagsCIds132h_L2

ratioPaire = []

dist_120h_132h_L2 = np.zeros((max(allTagsD_L2_new.values()) + 1, max(allTagsD_L2_new.values()) + 1))

for pat, cidL in startPattern.iteritems():
    if pat not in pattToRemove:
        patD = []
        for cid in cidL:
            descendants = dtis.extractDescendants(start, cid, stop)
            for dcid in descendants:
                dtag = cidToTag(dcid, nextPattern)
                if dtag is not None:
                    patD.append(allTagsD_L2_new[dtag])
                elif appendZero:
                    patD.append(0)
        dPatInd = set(patD)
        for dP in dPatInd:
            dist_120h_132h_L2[allTagsD_L2_new[pat], dP] = patD.count(dP) / float(len(patD)) 

# fobj = file("transitionGraph%s_%s_L2.pkl"%(start, stop), "w")
# cPickle.dump(dist_120h_132h_L2, fobj)
# fobj.close()


