import cPickle
import numpy as np
from scipy.spatial.distance import hamming
import matplotlib.pyplot as plt

from fileNames import atlas_pattern_fileNames_all
from allTagsDModule import allTagsD_L1_new, allTagsD_L2_new
from createTagsD import patterns10hAllTissue, patterns40hAllTissue, patterns96hAllTissue, patterns120hAllTissue, patterns132hAllTissue


def makePatternBinary(p, genes):
    bPat = []
    for g in genes:
        if g in p:
            bPat.append(1)
        else:
            bPat.append(0)
    return np.array(bPat)
        
allGenes = set()
allGenes.update(patterns10hAllTissue.keys())
allGenes.update(patterns40hAllTissue.keys())
allGenes.update(patterns96hAllTissue.keys())
allGenes.update(patterns120hAllTissue.keys())
allGenes.update(patterns132hAllTissue.keys())
allGenes -= set(["L1", "L2"])

allGenes = list(allGenes)
allGenes.sort()


dist = np.zeros((max(allTagsD_L1_new.values()) + 1, max(allTagsD_L1_new.values()) + 1))

for t0, c0 in allTagsD_L1_new.iteritems():
    if True:#t0 in tagsColomapId120h:
        for t1, c1 in allTagsD_L1_new.iteritems():
            if True: #t1 in tagsColomapId132h:
                b0 = makePatternBinary(t0, allGenes)
                b1 = makePatternBinary(t1, allGenes)
                dist[c0, c1] = hamming(b0, b1)
                v1 = len(t0 - t1) + len(t1 - t0)
                assert (v1 - (dist[c0, c1] * len(allGenes))) < 1


# fobj = file("hammingDist_L1.pkl", "w")
# cPickle.dump(dist, fobj)
# fobj.close()
# 
# 
# p1 = plt.matshow(dist * len(allGenes))
# plt.colorbar(p1, ticks=[0, 8, np.max(dist) * len(allGenes)], orientation='vertical')
# plt.xlim([0.5, max(allTagsD_L1_new.values()) + 0.5])
# plt.ylim([max(allTagsD_L1_new.values()) + 0.5, 0.5])
# 
# plt.show()
 

