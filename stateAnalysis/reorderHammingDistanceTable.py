import numpy as np
import cPickle
import matplotlib.pyplot as plt

from allTagsDModule import allTagsD_L1_new
from hierarchicalClustering import reorderedPatterns
from computeHammingDistances_L1 import dist


__doc__ = """
reorders hamming distances table according to clustering indexes and plots the 
hamming distance table with new indexes. If a the patterns order has change 
mannualy first set mannualSet to True and then set the mannual ordering 
to newPatternsOrder.
"""

mannualSet = True

if mannualSet:
    newPatternsOrder = [2, 3, 6, 7, 10, 18, 26, 21, 12, 8, 11, 28, 27, 16, 17, 9, 1, 31, 5, 4, 20, 30, 29, 19, 25, 13, 22, 24, 15, 23, 14] # set mannualy after reordering by Jan
else:
    newPatternsOrder = reorderedPatterns 

genesInPatterns = set()
for pat in allTagsD_L1_new:
    genesInPatterns.update(pat)
    
    
allTagsDInverse = dict((v, k) for k, v in allTagsD_L1_new.iteritems())

genesInPatterns = list(genesInPatterns)

newPatternsOrderDict = dict((i + 1, newPatternsOrder[i]) for i in xrange(len(newPatternsOrder)))

distNewIndexsFromClustering = np.zeros((max(allTagsD_L1_new.values()) + 1, max(allTagsD_L1_new.values()) + 1))

for t0, c0 in allTagsD_L1_new.iteritems():
    for t1, c1 in allTagsD_L1_new.iteritems():
        distNewIndexsFromClustering[c0, c1] = dist[newPatternsOrderDict[c0], newPatternsOrderDict[c1]]#hamming(b0, b1)
                
p1 = plt.matshow(distNewIndexsFromClustering * (len(genesInPatterns) + 1))
plt.colorbar(p1, ticks=[0, 8, np.max(distNewIndexsFromClustering) * (len(genesInPatterns) + 1)], orientation='vertical')

plt.xlim([0.5, max(allTagsD_L1_new.values()) + 0.5])
plt.ylim([max(allTagsD_L1_new.values()) + 0.5, 0.5])
plt.xticks([i + 1 for i in xrange(len(newPatternsOrder))], newPatternsOrder, fontsize = 14)
plt.yticks([i + 1 for i in xrange(len(newPatternsOrder))], newPatternsOrder, fontsize = 14)
plt.show()
    
                
