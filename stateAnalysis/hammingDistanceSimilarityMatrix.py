import numpy as np
import matplotlib.pyplot as plt
from computeHammingDistances_L1 import dist, allGenes
from allTagsDModule import allTagsD_L1_new, allTagsD_L2_new


p1 = plt.matshow(dist * len(allGenes))
plt.colorbar(p1, ticks=[0, 8, np.max(dist) * len(allGenes)], orientation='vertical')
plt.xlim([0.5, max(allTagsD_L1_new.values()) + 0.5])
plt.ylim([max(allTagsD_L1_new.values()) + 0.5, 0.5])

plt.show()
 