import numpy as np
import matplotlib.pyplot as plt
from allTagsDModule import allTagsD_L1_new, tagsCodeTuplesL1


genesInPatterns = set()
for pat in allTagsD_L1_new:
    genesInPatterns.update(pat)
    
    
genesInPatterns = list(genesInPatterns)
genesInPatterns.sort()
genesInPatternsD = dict()

counter = 0
for g in genesInPatterns:
    genesInPatternsD[g] = counter
    counter += 1 
    

def patternToBinary(pat):
    b = np.zeros(len(genesInPatterns))
    for g in pat:
        b[genesInPatternsD[g]] = 1
    return b


newMat = np.zeros((len(allTagsD_L1_new) + 1, len(genesInPatterns)))
    
for c, t in tagsCodeTuplesL1:
    res = np.uint(patternToBinary(t))
    newMat[c, :] = patternToBinary(t)
    colorPat = []
    for i in xrange(len(genesInPatterns)):
        if res[i] > 0:
            colorPat.append("G " + genesInPatterns[i])
        else:
            colorPat.append("R " + genesInPatterns[i])

p1 = plt.matshow(newMat[1:, :], cmap=plt.cm.gray_r, alpha = 0.75)
plt.xticks([item - 0.5 for item in xrange(0, len(genesInPatterns))], genesInPatterns, rotation='vertical', fontsize = 14)
plt.yticks([item - 0.5 for item in xrange(1, len(allTagsD_L1_new) + 1)], xrange(1, len(allTagsD_L1_new) + 1), fontsize = 14)

plt.grid(True)
plt.show()


 
