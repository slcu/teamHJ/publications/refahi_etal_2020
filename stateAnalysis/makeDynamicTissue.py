from atlasviewer.dtissue import DTissue
from atlasviewer.avtiff import voxelDimensionsFromTiffFile
from fileNames import  YR_01_path_timepoints, YR_01_segmentations_file_names, YR_01_path, YR_01_tracking_file_names
import cPickle

timePointNames = ["%dh"%tp for tp in YR_01_path_timepoints]
realTimes = [0.0, 11.216666666666667, 19.266666666666666, 27.466666666666665, 35.133333333333333, 42.966666666666669, 50.550000000000004, 60.38333333333334, 66.333333333333343, 74.650000000000006, 84.63333333333334, 91.466666666666669, 97.983333333333334, 106.53333333333333, 115.25, 123.93333333333334, 131.05000000000001, 135.26666666666668]
realTimePoints = dict(zip(timePointNames, realTimes))

resolutions = {tp: voxelDimensionsFromTiffFile(fn) for tp, fn  in YR_01_segmentations_file_names.iteritems() if tp != "137h"}
dtis = DTissue(timePointNames)
dtis.setSegmentationFiles(YR_01_segmentations_file_names)

dtis.setResolutions(resolutions)
dtis.setRealTimepoints(realTimePoints)
dtis.extractLabels()
dtis.setTrackingFiles(YR_01_tracking_file_names)
dtis.computeVolumes()

fobj = file("FM1_dtissue.tis", "w")
cPickle.dump(dtis, fobj)
fobj.close()


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    growth = dtis.computeGrowthRate("120h", "132h" )
    plt.hist(growth.values(), bins = 30)
    plt.grid(True)
    plt.xlabel("$\mu m^3 /h$", fontsize = 20)
    plt.show()

