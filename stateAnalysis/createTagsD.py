import cPickle
import numpy as np
from atlasviewer.image import makepatternCellsValues
from fileNames import atlas_pattern_fileNames_L1, atlas_pattern_fileNames_L2, atlas_pattern_fileNames_L1L2, atlas_pattern_fileNames_all

__doc__ = """
creates a dictionary of all patterns in all time points for L1 named allTagsD_L1 and for L1 named allTagsD_L2
"""


def createDicts(patFileName10h_L1, patFileName40h_L1, patFileName96h_L1, patFileName120h_L1, patFileName132h_L1):
    
    fobj = file(patFileName10h_L1)
    patterns10h_L1 = cPickle.load(fobj)
    fobj.close()
    
    fobj = file(patFileName40h_L1)
    patterns40h_L1 = cPickle.load(fobj)
    fobj.close()
    
    fobj = file(patFileName96h_L1)
    patterns96h_L1 = cPickle.load(fobj)
    fobj.close()
    
    fobj = file(patFileName120h_L1)
    patterns120h_L1 = cPickle.load(fobj)
    fobj.close()
    
    fobj = file(patFileName132h_L1)
    patterns132h_L1 = cPickle.load(fobj)
    fobj.close()
    
    # patterns = [patterns10h, patterns40h, patterns96h, patterns120h, patterns132h]
    patterns_L1 = [patterns10h_L1, patterns40h_L1, patterns96h_L1, patterns120h_L1, patterns132h_L1]
    
    pat10hKeys_L1 = set(patterns10h_L1.keys()) - set(["L1", "L2"])
    pat40hKeys_L1 = set(patterns40h_L1.keys()) - set(["L1", "L2"])
    pat96hKeys_L1 = set(patterns96h_L1.keys()) - set(["L1", "L2"])
    pat120hKeys_L1 = set(patterns120h_L1.keys()) - set(["L1", "L2"])
    pat132hKeys_L1 = set(patterns132h_L1.keys()) - set(["L1", "L2"])
    
    patternsKeys_L1 = [pat10hKeys_L1, pat40hKeys_L1, pat96hKeys_L1, pat120hKeys_L1, pat132hKeys_L1]
     
    cellsValues10h_L1, tagsColomapId10h_L1, tagsCIds10h_L1 = makepatternCellsValues(patterns10h_L1, pat10hKeys_L1)
    cellsValues40h_L1, tagsColomapId40h_L1, tagsCIds40h_L1 = makepatternCellsValues(patterns40h_L1, pat40hKeys_L1)
    cellsValues96h_L1, tagsColomapId96h_L1, tagsCIds96h_L1 = makepatternCellsValues(patterns96h_L1, pat96hKeys_L1)
    cellsValues120h_L1, tagsColomapId120h_L1, tagsCIds120h_L1 = makepatternCellsValues(patterns120h_L1, pat120hKeys_L1)
    cellsValues132h_L1, tagsColomapId132h_L1, tagsCIds132h_L1 = makepatternCellsValues(patterns132h_L1, pat132hKeys_L1)
    
    allTagsD = {}
    
    tagsToRemove = [frozenset(['ATML1']), frozenset(['ATML1', 'STM'])]
    T = [tagsColomapId10h_L1.keys(), tagsColomapId40h_L1.keys(), tagsColomapId96h_L1.keys(), tagsColomapId120h_L1.keys(), tagsColomapId132h_L1.keys()]
    soFarTags = set()
    timesCodeTags = dict()
    
    counter = 1
    for tag in T:
        newTags2 = list([list(newt) for newt in tag] )
        newTags = []
        for nttt in newTags2:
            nttt.sort()
            newTags.append(nttt)
        
        newTags.sort()
        
        for tt in newTags:
            k1 = []
            t = frozenset(tt)
            if (t not in tagsToRemove) and (t not in soFarTags):
                allTagsD[t] = counter
                
                if t in tagsColomapId10h_L1.keys():
                    k1.append("10h")
                if t in tagsColomapId40h_L1.keys():
                    k1.append("40h")
                if t in tagsColomapId96h_L1.keys():
                    k1.append("96h")
                if t in tagsColomapId120h_L1.keys():
                    k1.append("120h")
                if t in tagsColomapId132h_L1.keys():
                    k1.append("132h")
                k2 = counter
                k = (tuple(k1), k2)
                timesCodeTags[k] = tt
                counter += 1
                soFarTags.add(t) 
    
    allTagsDInverse = dict((v, k) for k, v in allTagsD.iteritems())
    
    genesInPatterns = set()
    for pat in allTagsD:
        genesInPatterns.update(pat)
        
    genesInPatterns = list(genesInPatterns)
    genesInPatterns.sort()
    genesInPatternsD = dict()
    
    counter = 0
    for g in genesInPatterns:
        genesInPatternsD[g] = counter
        counter += 1 
        
    return allTagsD, genesInPatterns, timesCodeTags 
    
    

allTagsD_L1, genesInPatterns_L1, timesCodeTags_L1Before = createDicts(atlas_pattern_fileNames_L1["10h"], atlas_pattern_fileNames_L1["40h"], atlas_pattern_fileNames_L1["96h"], atlas_pattern_fileNames_L1["120h"], atlas_pattern_fileNames_L1["132h"])

allTagsD_L2, genesInPatterns_L2, timesCodeTags_L2Before = createDicts(atlas_pattern_fileNames_L2["10h"], atlas_pattern_fileNames_L2["40h"], atlas_pattern_fileNames_L2["96h"], atlas_pattern_fileNames_L2["120h"], atlas_pattern_fileNames_L2["132h"])


allTagsD_L1L2, genesInPatterns_L1L2, timesCodeTags_L1L2Before = createDicts(atlas_pattern_fileNames_L1L2["10h"], atlas_pattern_fileNames_L1L2["40h"], atlas_pattern_fileNames_L1L2["96h"], atlas_pattern_fileNames_L1L2["120h"], atlas_pattern_fileNames_L1L2["132h"])

allTagsD_all, genesInPatterns_all, timesCodeTags_allBefore = createDicts(atlas_pattern_fileNames_all["10h"], atlas_pattern_fileNames_all["40h"], atlas_pattern_fileNames_all["96h"], atlas_pattern_fileNames_all["120h"], atlas_pattern_fileNames_all["132h"])





allTagsD_L1_without_ATML1 = {}
counter = 0
for p, id in allTagsD_L1.iteritems():
    newP = p - set(["ATML1"])
    allTagsD_L1_without_ATML1[newP] = id
    if newP in allTagsD_L2.keys():
        counter += 1


#---- whole tissue

fobj = file(atlas_pattern_fileNames_all["10h"])
patterns10hAllTissue = cPickle.load(fobj)
fobj.close()
      
fobj = file(atlas_pattern_fileNames_all["40h"])
patterns40hAllTissue = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_all["96h"])
patterns96hAllTissue = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_all["120h"])
patterns120hAllTissue = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_all["132h"])
patterns132hAllTissue = cPickle.load(fobj)
fobj.close()



#---- L1

fobj = file(atlas_pattern_fileNames_L1["10h"])
patterns10hOnlyL1 = cPickle.load(fobj)
fobj.close()
      
fobj = file(atlas_pattern_fileNames_L1["40h"])
patterns40hOnlyL1 = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_L1["96h"])
patterns96hOnlyL1 = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_L1["120h"])
patterns120hOnlyL1 = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_L1["132h"])
patterns132hOnlyL1= cPickle.load(fobj)
fobj.close()


#---- L2

fobj = file(atlas_pattern_fileNames_L2["10h"])
patterns10hOnlyL2 = cPickle.load(fobj)
fobj.close()
      
fobj = file(atlas_pattern_fileNames_L2["40h"])
patterns40hOnlyL2 = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_L2["96h"])
patterns96hOnlyL2 = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_L2["120h"])
patterns120hOnlyL2 = cPickle.load(fobj)
fobj.close()

fobj = file(atlas_pattern_fileNames_L2["132h"])
patterns132hOnlyL2 = cPickle.load(fobj)
fobj.close()






if __name__ == "__main__":
    print "allTagsD_L1 = ", allTagsD_L1
    
    print "allTagsD_L2 = ", allTagsD_L2
    
    print "allTagsD_L1L2 = ", allTagsD_L1L2
    
    print "allTagsD_all = ", allTagsD_all

    patternsL1 = allTagsD_L1_without_ATML1.keys()
    patternsL2 = allTagsD_L2.keys()
    
    
    
    
    
    




